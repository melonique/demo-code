# Démo de code et de projets
CV: https://drive.google.com/open?id=1Mm-FmASrL3JUVOOn-9WUrzdfgK5lQ6BT

#### Compétences:
HTML5, CSS3, JavaScript ES6+, PHP,  Sass, jQuery, Knockout, React (Redux, Saga), Angular, Sitecore, Wordpress, Firebase, .Net, IIS, Apache, NGINX, MySQL, MongoDB, Google Analytics, Adobe Analytics, Tealium,  Jira, Git, TFS, Photoshop, Illustrator, Sketch, NodeJs, Gulp, Webpack ...

---

## Juin 2018: msj-admin-dev.firebaseapp.com
Mon application React la plus récente. Pour des raisons de confidntialité, je ne vous ai pas mis le code, mais vous invite a aller jouer sur la plateforme en développement. C'est la version avancé du CMS play.msj.world, qui utilise les utilisateurs et la base de donnée de Firebase.

User: demo@demo.com

Pass: demodemo

#### Technologies:
React, Redux, Saga, Firebase, StyledComponents

---

## Mars 2018: play.msj.world/demo3 (alpha)
C'est mon premier projet __React__ 100% fait par moi. C'est un CMS (pas encore sécurisé à cette étape) React qui utilise la database et le hosting de Firebase. J'ai beaucoup appris depuis et je ne fais déjà plus les choses de la même façon haha.
#### Technologies:
React, Redux, Saga, Firebase, StyledComponents

---

## janvier 2018: ia.ca/reee
J'ai fait la grande majorité des __calculateurs__ et __formulaires__ qu'on retrouve sur ia.ca. Celui-çi est le dernier que j'ai fait.
#### Technologies:
Knockout.js, Sitecore

---

## 2017/2018: iac.secureweb.inalco.com/ESMWPN20/public
Application __React__ faite en équipe avec plusieurs développeurs logiciels qui commençaient React. J'avais comme rôle de m'assurer que tout le monde gardait en tête les principes de UX et qu'on suive les maquettes à la lettre. J'ai donc fait, ou complété toutes les composantes react du formulaire. (boutons, collapse, inputs, modal, etc)
#### Technologies:
React, Redux, Saga, StyledComponents

---

## 2017: ia.ca/boussole
J'ai eu la chance de pouvoir concevoir une boussole financière 100% Front faite avec Knockout.js (parce que supporte ie8).

Je me suis occupée de la modèlisation des données, du controle des résultats personalisés selon les réponses et l'affichage et animation du "quiz".

J'ai vraiment aimé faire ce projet.
#### Technologies:
Knockout.Js, Sitecore

---


## 2017: 125.ia.ca, 125ans.ia.ca
C'est 2 sites avec des concours et des évènements. J'avais comme mendat de faire un "thème" de site 100% réutilisable dans Sitecore (un CMS orienté MVC) affin que des non-developpeurs puissent en maintenir le contenu et faire de nouvelles pages en réutilisant les composantes. Malhereusement, puisque les concours sont terminés, on ne peut pas voir les fonctionalités de votes et de quizz qu'il y a avait en 2017. Le quizz ressemblait un peut à ça: ia.ca/quiz
#### Technologies:
Sitecore

---


## 2016: goelan.com
C'est un wordpress (pas de code dsl) dont je me suis occupé de tout ce qui est techno. Création de serveur sur Digital ocean, sécurisation de celui-ci. Installation de wordpress et ses nombreux plugins. Découpage et intégration des maquettes basées sur le thème Avada.
Remarquez le génie de ma soeur qui a fait la stratégie de communication, rédigé tous les contenus et conceptualisé le UX.
#### Technologies:
Wordpress, Apache/Linux, MySql, etc.
