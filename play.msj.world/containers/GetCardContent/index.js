import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'

import {
  Card,
} from 'reactstrap'

import { CardEditForm } from '../'
import { CardContent, Loading, CardFooter } from '../../components'

import { ONE_CARD_REQUEST } from '../../constants/action-types'


const StyledCard = styled(Card)`
  margin: 0 auto;
  @media screen and (max-width: 768px){
    max-width: 100%;
    margin: 0 0.75rem;
  }
  >:first-child{
    border-radius: 4px 4px 0 0;
    + .card-body{
      padding-top: 1.25rem
    }
  }
  >:last-child{
    border-radius: 0 0 4px 4px;
  }
`

class GetCardContent extends Component {
  constructor(props) {
    super()
    this.oneCardRequest = props.oneCardRequest.bind(this)
  }
  // getCurrentCard(){
  //   const { appInfo, tokenId } = this.props
  //   this.oneCardRequest(appInfo, tokenId)
  //   this.forceUpdate()
  // }
  /*
  componentDidMount() {
    this.getCurrentCard()
  }

  componentWillReceiveProps() {
    if (!this.props.fetching.ONE_CARD) {
      this.getCurrentCard()
    }
  } */
  render() {
    const {
      fetching,
      currentCard,
      //  error,
      appId,
      edit,
    } = this.props

    return (
      <div className="App">
        {fetching.ONE_CARD ? (<Loading />) : ((
            (currentCard.tokenId || currentCard.cardId) && !edit &&
            <div>
              <StyledCard>
                <CardContent currentCard={currentCard} tokenId={currentCard.tokenId} appId={appId} />
                <CardFooter appId={appId} tokenId={currentCard.tokenId} />
              </StyledCard>
            </div>
        ))}
        {fetching.ONE_CARD ? (<div />) : ((
            (currentCard.tokenId || currentCard.cardId) && edit &&
            <div>
              <StyledCard>
                <CardEditForm currentCard={currentCard} tokenId={currentCard.tokenId} appId={appId} />
              </StyledCard>
            </div>
          ))}

      </div>
    )
  }
}

const mapStateToProps = state => ({
  fetching: state.root.fetching,
  error: state.root.error,
  currentCard: state.root.currentCard,
  allCards: state.root.allCards,
})

const mapDispatchToProps = dispatch => ({
  oneCardRequest: (appInfo, tokenId) => dispatch({ type: ONE_CARD_REQUEST, tokenId }),
})

GetCardContent.defaultProps = {
  currentCard: { parts: [{ type: 'base' }] },
  fetching: false
}

GetCardContent.propTypes = {
  currentCard: PropTypes.object,
  fetching: PropTypes.any,
}
export default connect(mapStateToProps, mapDispatchToProps)(GetCardContent)
