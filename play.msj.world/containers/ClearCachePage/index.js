import React from 'react'
import { Container, Button } from 'reactstrap'


const goBack = () => {
  window.history.back()
}
window.localStorage.clear()

const ClearCachePage = () => (
  <Container style={{ textAlign: 'center' }}>
    <br />
    <br />
    <h3>Vous avez vidé la cache des contenu!</h3> <br />
    <Button onClick={goBack}> Retourner à la page précédente</Button>
    <br />
    <br />
    <br />
  </Container>
)

export default ClearCachePage
