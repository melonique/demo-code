import React from 'react'
import styled from 'styled-components'
import { Container, Row, Col } from 'reactstrap'

import { Navigation } from '../../components'
import photo from '../../assets/img/landing-photo.jpg'
import pastilles from '../../assets/img/landing-pastilles.jpg'
import styles from '../../styles-variables'


const FondRose = styled.div`
  background-color: ${styles.color.pink};
`
const FondBleu = styled.div`
  background-color: #3472ed;
  margin-top: 1.25rem;
`
const TxtBlue = styled.span`
  color: ${styles.color.blue}
`
const TxtPink = styled.span`
  color: ${styles.color.pink}
`
const TxtYellow = styled.span`
  color: ${styles.color.yellow}
`

const WaveTop = styled.div`
  background: #fff;
  height: 50px;
  position: relative;
  &:before, &:after{
    border-bottom: 5px solid ${styles.color.pink};
  }
  &:before{
    content: "";
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 10px;
    background-size: 20px 40px;
    background-image: radial-gradient(circle at 10px -15px, transparent 20px, ${styles.color.pink} 21px);
  }
  &:after{
    content: "";
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 15px;
    background-size: 40px 40px;
    background-image: radial-gradient(circle at 10px 26px, ${styles.color.pink} 20px, transparent 21px);
  }
`

const WaveBottom = styled.div`
  background: ${styles.color.pink};
  height: 10px;
  position: relative;
  &:before, &:after{
    border-bottom: 5px solid #fff;
  }
  &:before{
    content: "";
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 10px;
    background-size: 20px 40px;
    background-image: radial-gradient(circle at 10px -15px, transparent 20px, #fff 21px);
  }
  &:after{
    content: "";
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 15px;
    background-size: 40px 40px;
    background-image: radial-gradient(circle at 10px 26px, #fff 20px, transparent 21px);
  }
`




const LandingPage = () => (
  <div>
    <Navigation />
    <FondRose>
      <WaveTop />
      <Container>
        <Row>
          <Col sm="12" md="4">
            <img src={photo} alt="Des enfants font une quête en plein air" style={{maxWidth: '100%', margin:'-40px 0 0'}} />
          </Col>
          <Col sm="12" md="8">
            <h1 style={{color: '#fff', marginTop: '1.25rem'}}>Nous recherchons nos premiers utilisateurs. </h1>
            <p style={{fontSize: '1.25rem', marginBottom: '1.25rem'}}>
              Vous aimez créer des chasses aux trésors, des rallyes d’apprentissage, imaginer des nouvelles expériences client?
              <br/>
              Laissez votre créativité s’exprimer et amusez-vous!
            </p>
           </Col>
        </Row>
      </Container>
      <WaveBottom />
    </FondRose>
    <Container>
      <div style={{borderColor: styles.color.green ,borderStyle: 'double', padding: '1.25rem', marginTop: '1.25rem'}}>
        <Row>
          <Col sm="12" md={{ size: 8, offset: 2 }}>
            <h2 style={{textAlign: 'center'}}>Détails de l’offre</h2>
            <p>Dans le cadre de sa phase Alpha, <TxtBlue>My</TxtBlue><TxtPink>Smart</TxtPink><TxtYellow>Journey</TxtYellow> offre un package tout inclus <span>à prix imbattable.</span> Ne ratez pas d’être le premier à tester notre solution qui exploite la technologie NFC pour créer des aventures!</p>
            <p style={{marginBottom: '1.25rem'}}>Notre campagne de financement participatif s’étend <TxtPink>du 10 mars au 20 avril</TxtPink>. Pour pouvoir lancer cette expérimentation, nous devons réunir un minimum de 25 clients. Afin d’assurer un suivi de qualité, les places sont limitées.</p>
          </Col>
        </Row>
        <Row>
          <Col sm="6">
            <h3>Nous vous offrons :</h3>
            <ul>
              <li>Des services de co-création d’un scénario à partir de votre contenu avec notre équipe de scénaristes et des game-designers.</li>
              <li>Un nombre illimité d’émetteurs en fonction des besoins du scénario. </li>
              <li>Le paramétrage des émetteurs. </li>
              <li>Le support technique durant toute la période de l’expérimentation.</li>
              <li>Durant cette phase d’expérimentation nous sommes ouverts à discuter de demandes particulières.</li>
            </ul>
          </Col>
          <Col sm="6">
            <h3>En contrepartie nous vous demandons :</h3>
            <ul>
              <li>Un investissement de 1000 dollars pour une quête,</li>
              <li>Ou un investissement de 5000 dollars pour la scénarisation et le paramétrage d’un nombre illimité de quêtes sur votre lieu.</li>
              <li>La possibilité d’enregistrer une vidéo avec vous avant, pendant et après le projet afin de recueillir vos impressions.</li>
              <li>Un accès au lieu pour pouvoir faire des journées d’observation et de prise de photos.</li>
              <li>Le droit de rédiger une étude de cas à partir de votre expérience (avec votre validation) et de l’utiliser à des fins de promotion du produit.</li>
            </ul>
          </Col>
        </Row>
      </div>
    </Container>
    <FondBleu>
      <Container>
        <Row>
          <Col sm="6">
            <img src={pastilles} style={{maxWidth: '100%'}} alt="Visualisation des pastilles dans leur packaging"/>
          </Col>
          <Col sm="6">
            <h2 style={{color: 'yellow', marginTop: '3rem'}}>Créez une quête interactive cet été!</h2>
            <p style={{color: '#fff', fontSize: '24px'}}>Pour une fraction du prix en échange de votre feedback, testez nos émetteurs Plug&Play.</p>
          </Col>
        </Row>
      </Container>
    </FondBleu>
  </div>
)

export default LandingPage
