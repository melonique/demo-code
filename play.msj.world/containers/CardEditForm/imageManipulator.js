
const resizeThisImage = (image, maxWidth, maxHeight, quality) => {
  const canvas = document.createElement('canvas')
  let { width, height } = image

    if (maxWidth && maxHeight) { //si on me donne des max
      if (width > height) {
      if (width > maxWidth) {
        height = Math.round((height * maxWidth) / width)
        width = maxWidth
      }
    } else if (height > maxHeight) {
      width = Math.round((width * maxHeight) / height)
      height = maxHeight
    }
  }

  canvas.width = width
  canvas.height = height

  const ctx = canvas.getContext('2d')

  ctx.fillStyle = '#FFF'
  ctx.fillRect(0, 0, canvas.width, canvas.height)
  ctx.drawImage(image, 0, 0, width, height)
  return canvas.toDataURL('image/jpeg', quality)
}

const resizeImage = (file, maxWidth, maxHeight, fn) => {
  const reader = new FileReader()
  reader.readAsDataURL(file)
  reader.onload = (event) => {
    const dataUrl = event.target.result

    const image = new Image()
    image.src = dataUrl
    image.onload = () => {
      const resizedDataUrl = resizeThisImage(image, maxWidth, maxHeight, 1)
      fn(resizedDataUrl)
    }
  }
}
export default resizeImage
