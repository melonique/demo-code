import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
// eslint-disable-next-line
import styled, { css } from 'styled-components'
import { Button, Form, Label, Input, FormGroup, Card, CardBody, CardTitle, Alert, Row, Col } from 'reactstrap'
import {
  PartButtonForm,
  PartTextForm,
  PartAudioForm,
  PartPictureUrlForm,
  PartPictureForm,
  PartVideoForm,
  PartVideoYoutubeForm,
  PartTypeForm,
  PartRedirectForm,
  Pastille
} from '../../components'

import resizeImage from './imageManipulator'
import { UPDATE_CARD_REQUEST } from '../../constants/action-types'

function arrayMove(arr, oldIndex, newIndex) {
  if (newIndex >= arr.length) {
      var k = newIndex - arr.length + 1
      while (k--) {
          arr.push(undefined)
      }
  }
  arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0])
  return arr
}

const StyledCardBody = styled(CardBody)`
  + .card-body{
    padding-top:0;
  }
`
const StyledBtnDiv = styled.div`
  button{
    display: block;
    width: 36px;
    height: 36px;
    padding: 0;
    text-align: center;
    margin: 10px;
  }
  .btn-pink{
    margin: 0 10px 33px;
  }
`

class cardBasicForm extends Component {
  constructor(props) {
    super()
    this.state = {
      appId: props.appId,
      title: props.currentCard.title,
      tokenId: props.tokenId,
      parts: props.currentCard.parts,
      cardId: props.currentCard.cardId,
      savedSuccess: false,
      saving: false,
    }
    // so the "this" in the functions work:
    this.handleChange = this.handleChange.bind(this)
    this.handlePartChange = this.handlePartChange.bind(this)
    this.handleFileChange = this.handleFileChange.bind(this)
    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handlePartCheckboxChange = this.handlePartCheckboxChange.bind(this)
    this.handleAddBlock = this.handleAddBlock.bind(this)
    this.handleRemoveBlock = this.handleRemoveBlock.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleMoveBlockUp = this.handleMoveBlockUp.bind(this)
    this.handleMoveBlockDown = this.handleMoveBlockDown.bind(this)
  }
  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value })
  }
  handlePartChange(e, i) {
    const currentParts = this.state.parts.slice()
    currentParts[i][e.target.id] = e.target.value
    this.setState({ parts: currentParts })
  }
  handlePartCheckboxChange(e, i) {
    const currentParts = this.state.parts.slice()
    currentParts[i][e.target.id] = e.target.checked
    console.log(currentParts)
    this.setState({ parts: currentParts })
  }
  handleFileChange(e, i) {
    const currentParts = this.state.parts.slice()
    currentParts[i][e.target.id] = e.target.files[0]
    this.setState({ parts: currentParts })
  }

  handlePictureChange(e, i) {
    const self = this
    const file = e.target.files[0]
    const currentParts = self.state.parts.slice()
    currentParts[i][e.target.id] = file
    self.setState({ parts: currentParts })

  }
  handleAddBlock(e) {
    e.preventDefault()
    const currentParts = this.state.parts.slice()
    currentParts.push({ type: '' })
    this.setState({ parts: currentParts })
  }
  handleResetBlock(e, i) {
    const currentParts = this.state.parts
    currentParts.splice(i, 1)
    this.setState({ parts: currentParts })
    this.handleAddBlock(e)
  }
  handleRemoveBlock(e, i) {
    const currentParts = this.state.parts
    currentParts.splice(i, 1)
    this.setState({ parts: currentParts })
  }
  handleMoveBlockUp(e, i) {
    const currentParts = this.state.parts
    const currentIndex = e.target.dataset.partindex
    const newIndex = e.target.dataset.partindex - 1
    const newPartsOrder = arrayMove(currentParts, currentIndex, newIndex)
    this.setState({ parts: newPartsOrder })
  }
  handleMoveBlockDown(e, i) {
    const currentParts = this.state.parts
    const currentIndex = e.target.dataset.partindex
    const newIndex = e.target.dataset.partindex + 1
    const newPartsOrder = arrayMove(currentParts, currentIndex, newIndex)
    this.setState({ parts: newPartsOrder })
  }
  handleSubmit(e) {
    this.setState({ saving: true })
    e.preventDefault()
    // find picture parts in parts, and compress each of them
    const currentParts = this.state.parts
    const parts = currentParts.map((part, i)=>{
      const currentPart = part;
      if(part.type === 'Picture' && part.isCompressed && part.file){
        resizeImage(part.file, 10, 10, (resizedDataUrl) => {
          currentPart.base64 = resizedDataUrl
        })
      } else if (part.type === 'Picture' && !part.isCompressed && part.file){
        resizeImage(part.file, false, false, (resizedDataUrl) => {
          currentPart.base64 = resizedDataUrl
        })
      }
      return currentPart
    })


    const { title, appId, tokenId } = this.state
    const { cardId } = this.props.currentCard
    this.props.onUpdateCard(
      title,
      appId,
      tokenId,
      parts,
      cardId
    )
  }
  render() {
    const {
      title
    } = this.state
    const {
      currentCard,
      appId,
      tokenId,
      fetching,
    } = this.props
    return (
      <div>
        <Form onSubmit={() => false}>
          <StyledCardBody style={{ background: '#f4f4f4', marginBottom: '1.25rem' }}>
            <table>
              <tbody>
                <tr>
                  <td style={{ paddingLeft: '1.25rem', paddingRight: '1.25rem', width: '100%' }}>
                    <FormGroup style={{ margin: '0' }}>
                      <Label style={{ display: 'none' }} htmlFor="title"><h4>Titre</h4></Label>
                      <Input
                        style={{ fontSize: '1.5rem' }}
                        type="text"
                        id="title"
                        value={title}
                        placeholder="Titre"
                        onChange={this.handleChange}
                      />
                    </FormGroup>
                  </td>
                  <td style={{ width: '70px' }}><Pastille currentCardTokenId={currentCard.tokenId} color="#cc0000" /></td>
                </tr>
              </tbody>
            </table>
          </StyledCardBody>
          <div style={{ padding: ' 0 1.25rem 1.25rem' }}>
            { this.state.parts.map((current, i) => (
              <div key={ `part-${i}` }>
                <Card style={{ background: '#f4f4f4' }}>
                  <CardBody style={{ position: 'relative' }}>
                    <Row>
                      <Col>
                        <CardTitle>Action {i + 1}</CardTitle>
                        <PartTypeForm partIndex={i} handleOnChange={this.handlePartChange} value={current.type} />
                        {current.type === 'Button' &&
                          <PartButtonForm partIndex={i} handleOnChange={this.handlePartChange} text={current.text} url={current.url} />
                        }
                        {current.type === 'Text' &&
                          <PartTextForm partIndex={i} handleOnChange={this.handlePartChange} text={current.text} />
                        }
                        {current.type === 'Redirect' &&
                          <PartRedirectForm partIndex={i} handleOnChange={this.handlePartChange} url={current.url} appId={appId} tokenId={tokenId} />
                        }
                        {current.type === 'Picture'  &&
                          <PartPictureForm
                            partIndex={i}
                            handlePictureChange={this.handlePictureChange}
                            handleOnChange={this.handlePartCheckboxChange}
                            url={current.url}
                            isCompressed={typeof current.isCompressed === 'undefined'? true : current.isCompressed}
                          />
                        }
                        {current.type === 'PictureUrl' &&
                          <PartPictureUrlForm partIndex={i} handleOnChange={this.handlePartChange} url={current.url} />
                        }
                        {current.type === 'Video' &&
                          <PartVideoForm partIndex={i} handleOnChange={this.handleFileChange} url={current.url} />
                        }
                        {current.type === 'VideoYoutube' &&
                          <PartVideoYoutubeForm partIndex={i} handleOnChange={this.handlePartChange} url={current.url} />
                        }
                        {current.type === 'Audio' &&
                          <PartAudioForm partIndex={i} handleOnChange={this.handleFileChange} url={current.url} />
                        }
                      </Col>
                      <StyledBtnDiv className="btns">
                        { (this.state.parts.length >= 2) &&
                          <Button color="pink" onClick={(e) => { this.handleRemoveBlock(e, i) }}>X</Button>
                        }
                        { (this.state.parts.length <= 1) &&
                          <Button color="pink" onClick={(e) => { this.handleResetBlock(e, i) }}>X</Button>
                        }
                        <Button color="secondary" disabled={i < 1} outline data-partindex={i} onClick={this.handleMoveBlockUp}> △ </Button>
                        <Button color="secondary" disabled={(i + 1) >= this.state.parts.length} outline data-partindex={i} onClick={this.handleMoveBlockDown}> ▽ </Button>
                      </StyledBtnDiv>
                    </Row>
                  </CardBody>
                </Card>

                <br />
              </div>
            ))}
            <Button color="success" outline block onClick={this.handleAddBlock}>Ajouter une action</Button>
          </div>

          <CardBody style={{ background: '#f4f4f4', paddingTop: '1.25rem' }}>
            { !fetching.UPDATE_CARD &&
              <div>
                <Button block color="green" type="submit" size="lg" onClick={this.handleSubmit}>Enregistrer</Button>
                <hr />
                <Button block color="blue" href={`/${appId}/${tokenId}`}>Voir</Button>
                <hr />
                <Row>
                  { tokenId > 1 &&
                    <Col>
                      <Button block color="info" outline href={`/${appId}/${tokenId - 1}/edit`}>Token précédent</Button>
                    </Col>
                   }
                  <Col>
                    <Button block color="info" outline href={`/${appId}/${tokenId + 1}/edit`}>Token suivant</Button>
                  </Col>
                </Row>
              </div>
            }
            {fetching.UPDATE_CARD === true &&
              <Alert color="success">
                Vos modifications sont en cours de sauvegarde
              </Alert>
            }

          </CardBody>
        </Form>
      </div>
    )
  }
}

cardBasicForm.propTypes = {
  onUpdateCard: PropTypes.func.isRequired,
  appId: PropTypes.string.isRequired,
  tokenId: PropTypes.any.isRequired
}
const mapStateToProps = state => ({
  fetching: state.root.fetching,
})
const mapDispatchToProps = dispatch => ({
  onUpdateCard: (title, appId, tokenId, parts, cardId) => dispatch({ type: UPDATE_CARD_REQUEST, title, appId, tokenId, parts, cardId })
})

const CardBasicForm = connect(mapStateToProps, mapDispatchToProps)(cardBasicForm)
export default CardBasicForm
