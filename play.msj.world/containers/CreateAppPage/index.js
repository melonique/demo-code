import React from 'react'
// import PropTypes from 'prop-types'
import { Container } from 'reactstrap'

import { CreateAppForm } from '../../components'

const CreateAppPage = () => {
  return (
    <Container>
      <CreateAppForm />
    </Container>
  )
}


CreateAppPage.propTypes = {
}
export default CreateAppPage
