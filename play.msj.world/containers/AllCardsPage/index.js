import React from 'react'
import PropTypes from 'prop-types'
import { Container } from 'reactstrap'
import { GetAppInfo, GetAllCardContent } from '../'


const ContentPage = ({ match }) => {
  const { appId } = match.params
  document.body.id = appId
  return (
    <Container>
      <GetAppInfo appId={appId} />
      <GetAllCardContent appId={appId} />
    </Container>
  )
}


ContentPage.propTypes = {
  match: PropTypes.object.isRequired,
}
export default ContentPage
