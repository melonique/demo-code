import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { Navigation } from '../../components'
import { APP_INFO_REQUEST } from '../../constants/action-types'

class GetCardContent extends Component {
  constructor(props) {
    super()
    this.appInfoRequest = props.appInfoRequest.bind(this)
    this.getAppInfo = this.getAppInfo.bind(this)
  }
  componentWillMount() {
    this.getAppInfo()
  }
  getAppInfo() {
    const { appId } = this.props
    this.appInfoRequest(appId)
    this.forceUpdate()
  }
  render() {
    const {
      appInfo,
      fetching
    } = this.props

    return (
      <div>
        {
          fetching.APP_INFO ? (<span />) : (
            appInfo.id && <Navigation appId={appInfo.id} numberOfCards={appInfo.tokenList.length} />
          )
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  fetching: state.root.fetching,
  error: state.root.error,
  appInfo: state.root.appInfo,
})

const mapDispatchToProps = dispatch => ({
  appInfoRequest: appId => dispatch({ type: APP_INFO_REQUEST, appId }),
})

GetCardContent.defaultProps = {
  appInfo: { },
  fetching: false,
  appId: undefined,
}

GetCardContent.propTypes = {
  appInfo: PropTypes.object,
  appId: PropTypes.any,
  fetching: PropTypes.any,
  appInfoRequest: PropTypes.func.isRequired,
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GetCardContent))
