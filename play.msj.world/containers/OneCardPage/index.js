import React from 'react'
import PropTypes from 'prop-types'
import { Container } from 'reactstrap'
import { GetCardContent, GetAppInfo } from '../../containers'


const OneCardPage = ({ match }) => {
  const { tokenId, appId, edit } = match.params
  document.body.id = appId
  return (
    <Container>
      <GetAppInfo appId={appId} />
      <GetCardContent appId={appId} tokenId={tokenId} edit={edit === 'edit'} />
    </Container>
  )
}


OneCardPage.propTypes = {
  match: PropTypes.object.isRequired,
}
export default OneCardPage
