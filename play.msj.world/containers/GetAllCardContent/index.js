import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'

import { CardBody, Button, Card } from 'reactstrap'
import { ALL_CARDS_REQUEST } from '../../constants/action-types'
import { tokenIsLocked, appIsLocked } from '../../constants/lockedAppsAndTokens'

import { CardContent, Loading } from '../../components'

const StyledCard = styled(Card)`
  width: 31%;
  height: 100%;
  margin: 1%;
  align-content: flex-start;

  @media (max-width: 992px) {
    width: 48%
  }

  @media (max-width: 768px) {
    width: 100%
  }

  >:first-child{
    border-radius: 4px 4px 0 0;
    + .card-body{
      padding-top: 1.25rem
    }
  }
  >:last-child{
    border-radius: 0 0 4px 4px;
  }


`
const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`

class GetAllCardContent extends Component {
  constructor(props) {
    super()
    this.onGetAllCards = props.onGetAllCards.bind(this)
  }
  // callGetAllCards(){
  //   const { appId } = this.props
  //   this.onGetAllCards(appId)
  // }
  /*
  componentDidMount() {
    const { appId } = this.props
    this.onRequestApi(appId)
    this.forceUpdate()
  } */
  /*
  componentWillReceiveProps() {
    if (!this.props.fetching) {
      const { appId } = this.props
      this.onRequestApi(appId)
      this.forceUpdate()
    }
  }
  */
  render() {
    const {
      fetching,
      //  error,
      appId,
      // edit,
      allCards
    } = this.props

    return (
      <div>
        { fetching.ALL_CARDS ? <span><Loading /></span> : (allCards &&
          <div>
            <Container>
              {allCards.map((currentCard, i) => (
                <StyledCard key={`cardlist-${i}`}>
                  <CardContent currentCard={currentCard} tokenId={currentCard.tokenId} appId={appId} isAllCards/>
                  { !tokenIsLocked(currentCard.tokenId) && !appIsLocked(appId) &&
                    <CardBody style={{ background: '#f4f4f4', paddingTop: '1.25rem' }}>
                      <Button style={{ float: 'left' }} color="blue" href={`/${appId}/${currentCard.tokenId}`}>Voir</Button>
                      <Button style={{ float: 'right' }} color="green" href={`/${appId}/${currentCard.tokenId}/edit`}>Modifier</Button>
                    </CardBody>
                  }
                </StyledCard>
              ))}
            </Container>
          </div>
         ) }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  fetching: state.root.fetching,
  error: state.root.error,
  currentCard: state.root.currentCard,
  allCards: state.root.allCards,
})

const mapDispatchToProps = dispatch => ({
  onGetAllCards: (appId) => dispatch({ type: ALL_CARDS_REQUEST, appId })
})

GetAllCardContent.defaultProps = {
  fetching: false
}

GetAllCardContent.propTypes = {
}
export default connect(mapStateToProps, mapDispatchToProps)(GetAllCardContent)
