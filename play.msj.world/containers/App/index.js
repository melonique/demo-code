import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux'

import { OneCardPage, LandingPage, AllCardsPage, CreateAppPage, ClearCachePage } from '../'


import * as routes from '../../constants/routes'


const App = ({ history }) => (
  <ConnectedRouter history={history}>
    <div>
      <Switch>
        <Route
          exact
          path={routes.LANDING}
          component={() => <LandingPage />}
        />
        <Route
          exact
          path={routes.CLEAR}
          component={() => <ClearCachePage />}
        />
        <Route
          exact
          path={routes.CREATE_APP}
          component={CreateAppPage}
        />
        <Route
          exact
          path={routes.ALL}
          component={AllCardsPage}
        />
        <Route
          exact
          path={routes.EDIT}
          component={OneCardPage}
        />
        <Route
          exact
          path={routes.CONTENT}
          component={OneCardPage}
        />
        <Route
          exact
          path={routes.UNLINKEDCARD}
          component={OneCardPage}
        />
      </Switch>
      <footer style={{ fontWidth: '400', textAlign: 'center', margin: '1.25rem 0', fontSize: '0.9rem', opacity: '0.7' }}>
        ©2018 - Propulsé par <a href="http://mysmartjourney.co">MySmartJourney</a>
      </footer>
    </div>
  </ConnectedRouter>
)

export default App
