export const LANDING = '/'
export const CLEAR = '/clear'

// RealApp content:
export const ALL = '/:appId/'
export const CREATE_APP = '/createapp'
export const CONTENT = '/:appId/:tokenId'
export const EDIT = '/:appId/:tokenId/:edit'
export const UNLINKEDCARD = '/:appId/card/:tokenId'
