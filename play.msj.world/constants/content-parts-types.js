// button
// text
// picture
// picture-url
// video
// video-youtube
// audio
const youtubeParser = (url) => {
  const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/
  const match = url.match(regExp)
  return (match && match[7].length === 11) ? match[7] : false
}
export class Button {
  constructor() {
    this.text = ''
    this.url = ''
  }
}
export class Text {
  constructor() {
    this.text = ''
  }
}
export class Picture {
  constructor() {
    this.url = ''
  }
}
export class PictureUrl {
  constructor() {
    this.url = ''
  }
}
export class Video {
  constructor() {
    this.url = ''
  }
}
export class VideoYoutube {
  constructor() {
    this.url = ''
    // retirer le ID du url
    this.id = youtubeParser(this.url)
  }
}
export class Audio {
  constructor() {
    this.url = ''
  }
}
