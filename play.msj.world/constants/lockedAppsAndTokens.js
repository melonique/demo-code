const lockedApps = ['mtlab', 'clubmedcancun', 'zombiesMTL', 'lacmegantic', 'mtl', 'amp2018']
const lockedTokens = ['c']

export const appIsLocked = (appId) => lockedApps.indexOf(appId) >= 0
export const tokenIsLocked = (tokenId) => lockedTokens.indexOf(tokenId) >= 0
