const autoplayVideos = ['clubmedcancun']

export const videoIsAutoplay = (appId) => autoplayVideos.indexOf(appId) >= 0
