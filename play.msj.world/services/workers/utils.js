export function readFileExtension(filename) {
  return filename.split('.').pop()
}
export function youtubeParser(url) {
  const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/
  const match = url.match(regExp)
  return (match && match[7].length === 11) ? match[7] : false
}
export function findCardLinkByTokenId(appCardLinks, tokenId) {
  const CardId = appCardLinks.filter((link, i) => {
    link.index = i
    return link.tokenId.toString() === tokenId.toString()
  })
  return CardId[0]
}
