import firebase from 'firebase'
import ReduxSagaFirebase from 'redux-saga-firebase'

/*
  PROD - PLAY
 */

const myFirebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyCTQ8cnnDtf-lEGLaJrzKsSWo2DuE8ct6U',
  authDomain: 'mysmartjourney-demo.firebaseapp.com',
  databaseURL: 'https://mysmartjourney-demo.firebaseio.com',
  projectId: 'mysmartjourney-demo',
  storageBucket: 'mysmartjourney-demo.appspot.com',
  messagingSenderId: '388415546504'
})

const rsFirebase = new ReduxSagaFirebase(myFirebaseApp)

export default rsFirebase
