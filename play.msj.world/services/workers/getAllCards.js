
import { call, put, select } from 'redux-saga/effects'
import { selectAppInfo } from '../selectors'

import {
  ALL_CARDS_ERROR,
  ALL_CARDS_ONE_SUCCESS,
} from '../../constants/action-types'

import rsFirebase from './rsf'

function* getAllCards() {

  const appInfo = yield select(selectAppInfo)
  yield appInfo


  for (let cardLink of appInfo.tokenCardLinks){
    const cardPathId = `cards/${appInfo.id}/${cardLink.cardId}`
    const cachedCurrentCard = JSON.parse(localStorage.getItem(cardPathId))
    if(cachedCurrentCard && cardLink.update <= cachedCurrentCard.updateTime){
      // console.log('current card in localStorage for One Card of All Cards')
        yield put({
          type: ALL_CARDS_ONE_SUCCESS,
          currentCard : cachedCurrentCard
        })
    }
    else{
      try {
        // console.log('getting content from DB for One Card of All Cards')
        const currentCard = yield call(rsFirebase.database.read, cardPathId)

        yield currentCard
        // console.log('added content to localStorage for One Card of All Cards')
        localStorage.setItem(cardPathId, JSON.stringify(currentCard))

        yield put({
          type: ALL_CARDS_ONE_SUCCESS,
          currentCard: currentCard
        })
      }
      catch (error) {
        yield put({ type: ALL_CARDS_ERROR, error })
      }
    }
  }
/*
  try {
    const allCards = yield call(rsFirebase.database.read, `cards/${appId}/`)

    yield put({
      type: ALL_CARDS_SUCCESS,
      allCards,
      tokenId,
      appId,
    })
  } catch (error) {
    yield put({ type: ALL_CARDS_ERROR, error })
  }
  */
}


export default getAllCards;
