
import { call, put, select, all, fork } from 'redux-saga/effects'
import UUID from 'uuid-js'

import { readFileExtension, youtubeParser, findCardLinkByTokenId } from './utils'
import { selectAppInfo } from '../selectors'

import {
  UPDATE_CARD_SUCCESS,
  UPDATE_CARD_ERROR,
} from '../../constants/action-types'

import { uploadFile64, uploadFile } from './fileManager'

import rsFirebase from './rsf'


const generateNewCard = (data) => {
  const {
    title,
    tokenId,
    parts,
    cardId,
    updateTime
  } = data
  return {
    title,
    parts,
    updateTime,
    cardId,
    tokenId,
  }
}


function* uploadPartFiles(data) {
  const {
    appId,
    parts,
  } = data

  // si ya une part qui a une file (type="Picture")
  // on upload l'image pi on prend l'url pi on l'ajoute a la part comme "url"
  // pi on retire le "file" du part
  for (let part of parts) {
    // si c'est une image, on save pas l'image, on utilise le base64 généré par le canvas
    if(part.type === 'Picture' ){
      // save as base64
      if(part.file){
        const uuid4 = UUID.create()
        const fileExtension = readFileExtension(part.file.name)
        const fileName = `${uuid4.toString()}.${fileExtension}`
        const url = `/apps/${appId}/${fileName}`
        const downloadUrl = yield uploadFile64(part.base64.slice(23), url)
        yield downloadUrl
        part.url = downloadUrl
        part.base64 = ''
      }
    }
    if ((part.type === 'Video' && part.file) || (part.type === 'Audio' && part.file)){
      debugger
        const uuid4 = UUID.create()
        const fileExtension = readFileExtension(part.file.name)
        const fileName = `${uuid4.toString()}.${fileExtension}`
        const url = `/apps/${appId}/${fileName}`
        const downloadUrl = yield uploadFile(part.file, url)
        yield downloadUrl
        part.url = downloadUrl
    }
    else if(part.type === 'VideoYoutube'){
      part.id = youtubeParser(part.url)
    }
  }
  return parts
}

// on update la card au cards de l'app
function* updateCardInfo({ appId, cardId }, myCard) {
  yield call(rsFirebase.database.update, `cards/${appId}/${cardId}`, myCard)
}

function* updateLastCardUpdateTime({ appId, updateTime }) {
  yield call(rsFirebase.database.update, `apps/${appId}/lastCardUpdateTime`, updateTime)
}

function* updateTokenCardLink(data) {
  const {
    appId,
    appInfo,
    tokenId,
    cardId,
    updateTime
  } = data
  const cardLinkIndex = findCardLinkByTokenId(appInfo.tokenCardLinks, tokenId).index
  const newCardLink = { cardId, tokenId, update: updateTime }

  yield call(rsFirebase.database.update, `apps/${appId}/tokenCardLinks/${cardLinkIndex}`, newCardLink)
}

function* updateOneCard(data) {
  const myDataNow = data
  const {
    appId,
    cardId,
  } = myDataNow

  myDataNow.updateTime = Date.now().toString()
  myDataNow.appInfo = yield select(selectAppInfo)

  try {
    const newCard = generateNewCard(data)
    const partsWithFilesHandeled = yield uploadPartFiles(myDataNow)
    myDataNow.parts = partsWithFilesHandeled

    yield all([
      fork(updateCardInfo, myDataNow, newCard),
      fork(updateLastCardUpdateTime, myDataNow),
      fork(updateTokenCardLink, myDataNow),
    ])

    yield put({
      type: UPDATE_CARD_SUCCESS,
      appId,
      cardId,
      newCard,
    })
  } catch (error) {
    yield put({ type: UPDATE_CARD_ERROR, error })
  }
}

export default updateOneCard
