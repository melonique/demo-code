import { call } from 'redux-saga/effects'
import rsFirebase from './rsf'

export function* downloadFile(path) {
  const url = yield call(rsFirebase.storage.getDownloadURL, path)
  yield url
  return url
}

export function* uploadFile64(string, url) {
  const task = yield call(rsFirebase.storage.uploadString, url, string, 'base64')
  yield task
  const path = yield downloadFile(url)
  yield path
  return path
}

export function* uploadFile(file, url) {
  const task = yield call(rsFirebase.storage.uploadFile, url, file)
  // Wait for upload to complete
  yield task
  // Do something on complete
  const path = yield downloadFile(url)
  yield path
  return path
}
