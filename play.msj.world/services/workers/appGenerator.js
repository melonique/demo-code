
const contactCardParts = () => (
  [
    {
      text: 'Nous sommes une petite équipe, mais nous sommes souvent en ligne! :-)\n\nIl y a de nombreuses façon de nous parler en temps réel ou de nous laisser un message.',
      type: 'Text',
    }, {
      text: 'Clavardez via Facebook',
      type: 'Button',
      url: 'https://www.facebook.com/My-Smart-Journey-NFC-Playsets-2227666707458148/'
    }, {
      text: 'En vidéoconférence',
      type: 'Button',
      url: 'http://appear.in/mysmartjourney'
    }, {
      text: 'Par téléphone',
      type: 'Button',
      url: 'tel:+14182645447'
    }, {
      text: 'Par courriel',
      type: 'Button',
      url: 'mailto:lopez@mysmartjourney.co'
    }
  ]
)

const oneLink = (tokenId, cardId, update = 0) => ({ cardId, tokenId, update })

const oneCard = (tokenId, cardId, date = 0) => {
  let title = ''
  let parts = [{ type: '' }]
  if (tokenId === 'c') {
    title = 'Contactez-nous'
    parts = contactCardParts()
  }
  return {
    cardId,
    createdTime: date,
    parts,
    title,
    tokenId,
    updateTime: date
  }
}
const createTokenCardLinks = (tokenList, cardList, date) => {
  const list = tokenList.map((tokenId, i) => (
    oneLink(tokenId, cardList[i], date)
  ))
  return list
}

const createCardList = (nbrOfCards, withContact = true) => {
  const list = []
  for (let i = 1; i <= nbrOfCards; i++) {
    list.push(`${i}`) // should be UUID but i need it ordered
  }
  if (withContact) {
    list.push('999999') // should be UUID but i need it ordered
  }
  return list
}

const createTokenList = (nbrOfTokens, withContact = true) => {
  const list = []
  for (let i = 1; i <= nbrOfTokens; i++) {
    list.push(i)
  }
  if (withContact) {
    list.push('c')
  }
  return list
}

const createAllCardsObject = (tokenCardLinks, date) => {
  const list = tokenCardLinks.map((link) => {
    return oneCard(link.tokenId, link.cardId, date)
  })
  const object = {}
  list.map((card) => {
    object[card.cardId] = card
    return card
  })
  return object
}


/*
  let promoApp = createApp('Promo', 'promo', 25) // mettre a 40 si ca marche bien
*/
const createApp = (name, id, nbrOfTokens, withContact = true) => {
  const date = Date.now()
  const tokenList = createTokenList(nbrOfTokens, withContact)
  const cardList = createCardList(nbrOfTokens, withContact)
  const tokenCardLinks = createTokenCardLinks(tokenList, cardList, date)

  let apps = {}
  let cards = {}

  // create the app object
  apps[id] = {
    id,
    name,
    lastCardUpdateTime: date,
    cardList,
    tokenList,
    tokenCardLinks,
  }
  // create the cards object
  cards[id] = createAllCardsObject(tokenCardLinks, date)

  return {
    apps,
    cards
  }
}

export default createApp
