import { takeLatest, call, put, select } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import rsFirebase from './rsf'
import getOneCard from './getOneCard'
import getAllCards from './getAllCards'
import getAppInfo from './getAppInfo'
import updateOneCard from './updateOneCard'
import createOneApp from './createOneApp'

import { selectRouterPath } from '../selectors'

import {
  ONE_CARD_REQUEST,
  ALL_CARDS_REQUEST,
  APP_INFO_REQUEST,
  APP_INFO_SUCCESS,

  ONE_CARD_ERROR,
  ADD_CARD,
  UPDATE_CARD_REQUEST,
  CREATE_APP_REQUEST,
  UPDATE_CARD_SUCCESS,
} from '../../constants/action-types'

// takeLatest : act on the latest action dispatched.
// call: run a function....
// put: dispatch an action

function* addCard(data) {
  const {
    title,
    appId,
    tokenId,
    parts,
  } = data.payload

  try {
    const key = yield call(rsFirebase.database.create, `apps/${appId}/cards`, { title, tokenId, parts })
    yield key
    // const allCards = yield call(rsFirebase.database.read, `apps/${appId}/cards`)
    // yield put({
    //   type: ONE_CARD_SUCCESS,
    //   allCards,
    //   tokenId,
    //   appId,
    // })
  } catch (error) {
    yield put({ type: ONE_CARD_ERROR, error })
  }
}

/**
  THIS IS SO UGLY YOU HAVE TO CHANGE IT WHEN THE URLS CHANGES
 */
function* getOneOrAllCards() {
  const currentRoute = yield select(selectRouterPath)
  const route = currentRoute.split('/').join(' ').trim().split(' ')

  if (route.length === 1) { // only appId
    yield put({ type: ALL_CARDS_REQUEST })
  } else if (route.length === 2) {  // appId and tokenId
    yield put({ type: ONE_CARD_REQUEST, tokenId: route[1] })
  } else if (route.length === 3 && route[2] === 'edit') { //appId, tokenId, action
    yield put({ type: ONE_CARD_REQUEST, tokenId: route[1] })
  } else if (route.length === 3 && route[2] !== 'edit') { //appId, card, cardId
    yield put({ type: ONE_CARD_REQUEST, tokenId: route[2] })
  }
}

function* updateCardSuccess(data) {
  // redirect
  const { appId, cardId } = data
  const cardPathId = `cards/${appId}/${cardId}`
  localStorage.removeItem(cardPathId)
  yield put(push(`/${appId}/${cardId}`))
}
// watcher saga:
// watches for actions dispatched to the store
// then starts the worker saga
function* watcherSaga() {
  yield takeLatest(APP_INFO_REQUEST, getAppInfo)
  yield takeLatest(APP_INFO_SUCCESS, getOneOrAllCards)
  yield takeLatest(ONE_CARD_REQUEST, getOneCard)
  yield takeLatest(ALL_CARDS_REQUEST, getAllCards)

  yield takeLatest(ADD_CARD, addCard)
  yield takeLatest(UPDATE_CARD_REQUEST, updateOneCard)
  yield takeLatest(CREATE_APP_REQUEST, createOneApp)
  yield takeLatest(UPDATE_CARD_SUCCESS, updateCardSuccess)
}
export default watcherSaga
