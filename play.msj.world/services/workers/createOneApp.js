
import { call, put } from 'redux-saga/effects'

import {
  CREATE_APP_SUCCESS,
  CREATE_APP_ERROR
} from '../../constants/action-types'

import createApp from './appGenerator'

import rsFirebase from './rsf'


function* createOneApp(data) {
  const {
    name,
    appId,
    numberOfTokens,
    contactCard,
  } = data
  const app = createApp(name, appId, parseInt(numberOfTokens, 10), contactCard)

  try {
    // on ajoute les cards a la DB
    const Cards = yield call(rsFirebase.database.patch, 'cards/', app.cards)
    yield Cards
    // on ajoute l'app a la DB
    const Apps = yield call(rsFirebase.database.patch, 'apps/', app.apps)
    yield Apps

    yield put({ type: CREATE_APP_SUCCESS })
  } catch (error) {
    yield put({ type: CREATE_APP_ERROR, error })
  }
}

export default createOneApp
