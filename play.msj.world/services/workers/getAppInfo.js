import { call, put } from 'redux-saga/effects'

import {
  APP_INFO_SUCCESS,
  APP_INFO_ERROR,
} from '../../constants/action-types'

import rsFirebase from './rsf'


/**
 * 1. vérifier si il y a de la cache
 * 2. si oui, aller chercher la date d'update d'app
 * 3. au besoin update it
 */

function* getAppUpdateDate(appInfoPath) {
  const appLatestUpdate = yield call(rsFirebase.database.read, `${appInfoPath}/lastCardUpdateTime`)
  yield appLatestUpdate
  return appLatestUpdate
}


function* gatAppInfo({ appId, tokenId }) {
  const appInfoPath = `apps/${appId}`
  const appLatestUpdate = yield getAppUpdateDate(appInfoPath)
  yield appLatestUpdate
  const cachedCurrentAppInfo = JSON.parse(localStorage.getItem(appInfoPath))
  // si j'ai de la cache, et que la date du serveur n'est pas plus récente que la local, on utilise la local
  if (cachedCurrentAppInfo && appLatestUpdate <= cachedCurrentAppInfo.lastCardUpdateTime) {
    // console.log('used data from localStorage for AppInfo')
    yield put({
      type: APP_INFO_SUCCESS,
      appInfo: cachedCurrentAppInfo
    })
  } else {
    try {
      // console.log('getting content from DB for AppInfo')

      const appInfo = yield call(rsFirebase.database.read, appInfoPath)
      yield appInfo

      localStorage.setItem(appInfoPath, JSON.stringify(appInfo));
      // console.log('added content to localStorage for AppInfo')

      yield put({
        type: APP_INFO_SUCCESS,
        appInfo,
        tokenId,
        appId,
      })
    } catch (error) {
      yield put({ type: APP_INFO_ERROR, error })
    }
  }
}


export default gatAppInfo;
