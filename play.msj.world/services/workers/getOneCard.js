
import { call, put, select } from 'redux-saga/effects'
import { selectAppInfo } from '../selectors'
import {
  ONE_CARD_SUCCESS,
  ONE_CARD_ERROR,
} from '../../constants/action-types'

import rsFirebase from './rsf'

import { findCardLinkByTokenId } from './utils'

/**
 * 1. vérifier si il y a de la cache
 * 2. si oui, aller chercher la date d'update d'app
 * 3. au besoin update it
 */

function* getOneCard({ tokenId }) {
  console.log('tokenId', tokenId)
  const appInfo = yield select(selectAppInfo)
  yield appInfo
  const cardLink = findCardLinkByTokenId(appInfo.tokenCardLinks, tokenId)
  let cardPathId = ''
  console.log('cardLink', cardLink)
  if (typeof cardLink === "undefined") {   // on recupere une carte pas de token
    cardPathId = `cards/${appInfo.id}/${tokenId}`
  } else {
    cardPathId = `cards/${appInfo.id}/${cardLink.cardId}`
  }

  // const cachedCurrentCard = JSON.parse(localStorage.getItem(cardPathId))

  // si j'ai une card dans ma cache, et que sa date d'update n'est pas plus récente que celle qui était dans son link
  // if (cachedCurrentCard && cardLink.update <= cachedCurrentCard.updateTime) {
  //   // console.log('current card in localStorage for One Card')
  //   yield put({
  //     type: ONE_CARD_SUCCESS,
  //     currentCard: cachedCurrentCard
  //   })
  // } else {
    try {
      // console.log('getting content from DB for One Card')
      const currentCard = yield call(rsFirebase.database.read, cardPathId)

      yield currentCard
      console.log('added content to localStorage for One Card', currentCard)
      localStorage.setItem(cardPathId, JSON.stringify(currentCard))

      yield put({
        type: ONE_CARD_SUCCESS,
        currentCard
      })
    } catch (error) {
      yield put({ type: ONE_CARD_ERROR, error })
    }
 // }
}


export default getOneCard
