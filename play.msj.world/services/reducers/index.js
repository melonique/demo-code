import * as TYPE from '../../constants/action-types'

const initialState = {
  fetching: {
    ONE_CARD: true,
    APP_INFO: false,
    ALL_CARDS: false,
    UPDATE_CARD: false,
    error: null,
  },
  readyForCards: false,
  allCards: [],
  reload: false,
  error: null,
}
/* eslint-disable*/
export function rootReducer(state = initialState, action) {
    let currentFetching = state.fetching;

  switch (action.type) {
    // case TYPE.ADD_ARTICLE:
    //   return { ...state, articles: [...state.articles, action.payload] }

    // case TYPE.ADD_CARD:
    //   return { ...state, fetching: true, error: null , articles: [...state.articles, action.payload] }

    case TYPE.UPDATE_CARD_REQUEST:
      return {...state, fetching: { ...currentFetching, UPDATE_CARD: true,  error: null} }

    case TYPE.UPDATE_CARD_SUCCESS:
      state.currentCard = action.newCard
      return { ...state, fetching: { ...currentFetching, UPDATE_CARD: false,  error: null} }
    case TYPE.UPDATE_CARD_ERROR:
      return { ...state, fetching: { ...currentFetching, UPDATE_CARD: false,  error: action.error} }


    case TYPE.CREATE_APP_REQUEST:
      return {...state, fetching: { ...currentFetching, CREATE_CARD: true,  error: null} }
    case TYPE.CREATE_APP_SUCCESS:
      return { ...state, fetching: { ...currentFetching, CREATE_CARD: false,  error: null} }
    case TYPE.CREATE_APP_ERROR:
      return { ...state, fetching: { ...currentFetching, CREATE_CARD: false,  error: action.error} }


    case TYPE.ALL_CARDS_REQUEST:
      return { ...state, fetching: { ...currentFetching, ALL_CARDS: true,  error: null}, error: null }

    case TYPE.ALL_CARDS_ONE_SUCCESS:
      let currentAllCards = state.allCards;
      currentAllCards.push(action.currentCard)

      return {
        ...state,
        fetching: { ...currentFetching, ALL_CARDS: false,  error: null},
        allCards: currentAllCards
      }

    case TYPE.ALL_CARDS_ERROR:
      return {...state, fetching: { ...currentFetching, ALL_CARDS: false, error: action.error}}


    /** @type {APP_INFO} [launch the getAppInfo SAGA and return its content to the state] */
    case TYPE.APP_INFO_REQUEST:
      return {...state, readyForCards: false, fetching: { ...currentFetching, APP_INFO: true,  error: null}, appId : action.appId}

    case TYPE.APP_INFO_SUCCESS:
      return {
        ...state,
        readyForCards: true,
        fetching: { ...currentFetching, APP_INFO: false, error: null },
        appInfo : action.appInfo}

    case TYPE.APP_INFO_ERROR:
      return {...state, fetching: { ...currentFetching, APP_INFO: false, error: action.error}}



    /** @type {ONE_CARD} [Launch the getOneCard SAGA and return its content to the state] */
    case TYPE.ONE_CARD_REQUEST:
      return { ...state, fetching: {...currentFetching, ONE_CARD : true}, error: null }

    case TYPE.ONE_CARD_SUCCESS:
      return {
        ...state,
        fetching: {...currentFetching, ONE_CARD : false},
        currentCard: action.currentCard
      }

    case TYPE.ONE_CARD_FAILURE:
      return { ...state, fetching: {...currentFetching, ONE_CARD : false, error: action.error} }




    default:
      return state
  }
}

/* eslint-enable*/