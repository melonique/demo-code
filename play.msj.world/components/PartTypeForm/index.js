import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup } from 'reactstrap'


/*
   <option value="Audio">Audio - téléverser</option>
 */
const PartTypeForm = ({ handleOnChange, partIndex, value }) => (
  <FormGroup>
    <Label htmlFor={`type-${partIndex}`}>Type de contenu</Label>
    <Input
      type="select"
      name={`type-${partIndex}`}
      id="type"
      value={value}
      onChange={e => (handleOnChange(e, partIndex))}
    >
      <option value="">Choisir un type</option>
      <option value="Button">Bouton d'action</option>
      <option value="Text">Texte</option>
      <option value="Picture">Image - téléverser</option>
      <option value="PictureUrl">Image - url</option>
      <option value="Video">Vidéo - téléverser</option>
      <option value="VideoYoutube">Vidéo Youtube</option>
      <option value="Audio">Audio</option>
      <option value="Redirect">Redirect</option>
    </Input>
  </FormGroup>
)

PartTypeForm.defaultProps = {
  value: ''
}
PartTypeForm.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired,
  value: PropTypes.string
}

export default PartTypeForm
