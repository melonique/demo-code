import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup, FormText } from 'reactstrap'


const PartRedirectForm = ({ handleOnChange, partIndex, url, appId, tokenId }) => (
  <FormGroup>
    <Label htmlFor={`url-${partIndex}`}>URL</Label>
    <Input
      type="text"
      name={`url-${partIndex}`}
      id="url"
      value={url}
      onChange={e => (handleOnChange(e, partIndex))}
    />
    <FormText color="muted">
      Un redirect redirige automatiquement l’utilisateur vers un l’URL lorsqu’il appose son téléphone sur un jeton. Il ne suffit que d’insérer le lien URL de votre choix dans la case prévue à cet effet.
      <br />
      Nous vous déconseillons fortement de mettre un contenu autre que le titre sur la page considérant qu’il ne sera probablement pas vu.
      <br />
      Dans le futur, si vous voulez modifier cette page à nouveau, vous pouvez en allant directement sur cette adresse: http://visit.mysmartjounrney.co/{appId}/{tokenId}/edit
    </FormText>
  </FormGroup>
)

PartRedirectForm.defaultProps = {
  url: ''
}
PartRedirectForm.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired,
  appId: PropTypes.string.isRequired,
  tokenId: PropTypes.string.isRequired,
  url: PropTypes.string
}

export default PartRedirectForm
