import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
// eslint-disable-next-line
import styled, { css } from 'styled-components'
import { Button, Form, Label, Input, FormGroup, Card, CardBody, CardTitle } from 'reactstrap'


import { CREATE_APP_REQUEST } from '../../constants/action-types'



const initialState = {
  appId: '',
  name: '',
  numberOfTokens: '',
  contactCard: true,
  saving: false,
}
class CreateAppForm extends Component {
  constructor() {
    super()
    this.state = initialState
    // so the "this" in the functions work:
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value })
  }
  handleSubmit(e) {
    this.setState({ saving: true })
    e.preventDefault()
    const {
      name,
      appId,
      numberOfTokens,
      contactCard,
    } = this.state
    this.props.onCreateApp(
      name,
      appId,
      numberOfTokens,
      contactCard
    )
    this.setState(initialState)
  }
  render() {
    const {
      name,
      appId,
      numberOfTokens,
    } = this.state
    return (
      <div>
        <br /><br />
        <Form onSubmit={() => false}>
          <Card>
            <CardBody>
              <CardTitle>Create a new app</CardTitle>
              <FormGroup>
                <Label htmlFor="name">Name of the app</Label>
                <Input
                  type="text"
                  name="name"
                  id="name"
                  value={name}
                  onChange={e => (this.handleChange(e))}
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="appId">Id (that will be the url)</Label>
                <Input
                  type="text"
                  name="appId"
                  id="appId"
                  value={appId}
                  onChange={e => (this.handleChange(e))}
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="numberOfTokens">Number of tokens</Label>
                <Input
                  type="number"
                  name="numberOfTokens"
                  id="numberOfTokens"
                  value={numberOfTokens}
                  onChange={e => (this.handleChange(e))}
                />
              </FormGroup>
              <FormGroup tag="fieldset">
                <Label> With contact card?</Label>
                <FormGroup check>
                  <Label check>
                    <Input type="radio" id="contactCard" name="contactCard" onChange={this.handleChange} value="true" checked />
                    Yes
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="radio" id="contactCard" name="contactCard" onChange={this.handleChange} value="false" />
                    No
                  </Label>
                </FormGroup>
              </FormGroup>
            </CardBody>

            <CardBody style={{ background: '#f4f4f4', paddingTop: '1.25rem' }}>
              <Button block color="green" type="submit" size="lg" onClick={this.handleSubmit}>Enregistrer</Button>
              { /* fetching.UPDATE_CARD === true &&
                <Alert color="success">
                  Vos modifications sont en cours de sauvegarde
                </Alert>
              */ }

            </CardBody>

          </Card>
        </Form>
      </div>
    )
  }
}

CreateAppForm.propTypes = {
  onCreateApp: PropTypes.func.isRequired,
}
const mapDispatchToProps = dispatch => ({
  onCreateApp: (name, appId, numberOfTokens, contactCard) => dispatch({ type: CREATE_APP_REQUEST, name, appId, numberOfTokens, contactCard })
})

const CardBasicForm = connect(null, mapDispatchToProps)(CreateAppForm)
export default CardBasicForm
