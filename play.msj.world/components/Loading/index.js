import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import imgPastille from '../../assets/svg/loading.svg'

const LoadingAnimation = styled.div`
  background-color: #fff7;
  background-image: url(${imgPastille});
  background-position: center;
  background-size: cover;
  border-radius: 50%;
  box-shadow: 0 0 7px rgba(0, 0, 0, .2);
  display: block;
  height: 80px;
  padding: 0;
  position: fixed;
  top: 100px;
  width: 80px;
  z-index: 999;
  left: calc(50% - 40px)
`


const Loading = () => (
  <LoadingAnimation />
)

Loading.defaultProps = {
  currentCardTokenId: ''
}
Loading.propTypes = {
  currentCardTokenId: PropTypes.any
}

export default Loading;
