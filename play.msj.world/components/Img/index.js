import React from 'react'
import PropTypes from 'prop-types'
import imgLoading from '../../assets/svg/loading-gray.svg'

import ProgressiveImage from 'react-progressive-image'

const imgStyle = {
  maxWidth: '100%',
  display: 'block',
  margin: '0 auto',
}

const Img = ({ url, alt, ...props }) => (
  <ProgressiveImage src={url} placeholder={imgLoading} {...props}>
    {(src, loading) => (
      <img style={{ opacity: (loading ? 0.5 : 1), ...imgStyle }} src={src} alt={alt} />
    )}
  </ProgressiveImage>
)

Img.defaultProps = {
  url: '',
  alt: ''
}
Img.propTypes = {
  url: PropTypes.string,
  alt: PropTypes.string
}

export default Img
