import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup } from 'reactstrap'


const PartButtonForm = ({ handleOnChange, partIndex, text , url}) => (
  <div>
    <FormGroup>
      <Label htmlFor={`text-${partIndex}`}>Texte du boutton</Label>
      <Input
        type="text"
        name={`text-${partIndex}`}
        id="text"
        value={text}
        placeholder="ex: Cliquez ici"
        onChange={e => (handleOnChange(e, partIndex))}
      />
    </FormGroup>
    <FormGroup>
      <Label htmlFor={`url-${partIndex}`}>Lien du boutton</Label>
      <Input
        type="text"
        name={`url-${partIndex}`}
        id="url"
        value={url}
        placeholder="ex: http://google.com"
        onChange={e => (handleOnChange(e, partIndex))}
      />
    </FormGroup>
  </div>
)



PartButtonForm.defaultProps = {
  text: '',
  url: ''
}
PartButtonForm.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired,
  text: PropTypes.string,
  url: PropTypes.string,
}

export default PartButtonForm
