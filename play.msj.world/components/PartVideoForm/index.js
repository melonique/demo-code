import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup } from 'reactstrap'


const PartVideoForm = ({ handleOnChange, partIndex, url }) => (
  <FormGroup className="button">
    <Label htmlFor={`file-${partIndex}`}>Téléverser un vidéo</Label>
    <Input
      type="file"
      accept="video/*"
      name={`file-${partIndex}`}
      id="file"
      onChange={e => (handleOnChange(e, partIndex))}
    />
  </FormGroup>
)


PartVideoForm.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired
}

export default PartVideoForm
