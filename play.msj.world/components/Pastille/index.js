import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import imgPastille from '../../assets/svg/pastille.svg'

const PastilleWrapper = styled.div`
  padding: 0;
  margin: 0;
  background-color: #fff7;
  background-image: url(${imgPastille});
  background-position: center;
  background-size: cover;
  border-radius: 50%;
  color: red;
  width: 70px;
  height: 70px;
  display: block;
  position: relative;
  align-items: center;
  text-align: center;
  font-weight: 300;
  overflow: hidden;
  box-shadow: 0 0 7px rgba(0, 0, 0, .2)

  ${props => props.blue && css`
    color: 2px solid #007bff;
  `}
`

const PastilleNumber = styled.span`
  color: #007bff;
  font-size: 35px;
  margin-top: 12px;
  display:block;
  width: 100%;
  text-transform: uppercase;

  ${props => props.blue && css`
  color: 2px solid #007bff;
  `}
`

const Pastille = props => (
  <PastilleWrapper>
    <PastilleNumber>
      {props.currentCardTokenId}
    </PastilleNumber>
  </PastilleWrapper>
)

Pastille.defaultProps = {
  currentCardTokenId: ''
}

Pastille.propTypes = {
  currentCardTokenId: PropTypes.any.isRequired
}

export default Pastille;
