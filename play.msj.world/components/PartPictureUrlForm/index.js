import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup } from 'reactstrap'


const PartPictureUrlForm = ({ handleOnChange, partIndex, url }) => (
  <FormGroup>
    <Label htmlFor={`url-${partIndex}`}>Adresse de l'image</Label>
    <Input
      type="text"
      name={`url-${partIndex}`}
      id="url"
      value={url}
      placeholder="ex: http://google.com/logo.png"
      onChange={(e) => (handleOnChange(e, partIndex))}
    />
  </FormGroup>
)


PartPictureUrlForm.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired
}

export default PartPictureUrlForm
