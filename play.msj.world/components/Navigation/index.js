import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container
} from 'reactstrap'

import * as routes from '../../constants/routes'
import logo from '../../assets/img/Logo.png'
import clubmedLogo from '../../assets/img/clubmed.png'
import lockNChargeLogo from '../../assets/img/LOKnCHARGE_Black.png'
import zombiesMTLLogo from '../../assets/img/zombiesMTL-Logo.png'
import lacmegLogo from '../../assets/img/logo-lacmeg.png'
import mtlLogo from '../../assets/img/mtl-logo.jpg'

const clubMedNav = () => (
  <Navbar color="faded" light expand="md" style={{ marginBottom: '-0.4rem' }}>
    <Container>
      <NavbarBrand>
        <img src={clubmedLogo} alt="ClubMed Cancun" style={{ width: 'auto', height: '40px', marginLeft: '-14px' }} />
      </NavbarBrand>
    </Container>
  </Navbar>
)
const lockNchargeNav = () => (
  <Navbar color="faded" light expand="md" style={{ marginBottom: '-0.4rem' }}>
    <Container>
      <NavbarBrand>
        <img src={lockNChargeLogo} alt="LockNcharge" style={{ width: 'auto', height: '40px', marginLeft: '-14px' }} />
      </NavbarBrand>
    </Container>
  </Navbar>
)

const zombiesMTLNav = () => (
  <Navbar dark expand="md" style={{ marginBottom: '-0.4rem' }}>
    <Container>
      <NavbarBrand>
        <img src={zombiesMTLLogo} alt="zombiesMTL" style={{ width: 'auto', height: '70px', marginLeft: '-14px' }} />
      </NavbarBrand>
    </Container>
  </Navbar>
)

const LacMegNav = () => (
  <Navbar dark expand="md" style={{ marginBottom: '-0.4rem' }}>
    <Container>
      <NavbarBrand>
        <img src={lacmegLogo} alt="Commission des arts et de la culture du patrimoine - Lac Mégantic" style={{ width: 'auto', height: '70px', marginLeft: '-14px', marginBottom: '10px' }} />
      </NavbarBrand>
    </Container>
  </Navbar>
)

const MtlNav = () => (
  <Navbar dark expand="md" style={{ marginBottom: '-0.4rem' }}>
    <Container>
      <NavbarBrand>
        <img src={mtlLogo} alt="Montréal" style={{ width: 'auto', height: '70px', marginLeft: '-14px' }} />
      </NavbarBrand>
    </Container>
  </Navbar>
)


const Header = styled.header`
    position: relative;
    max-height: 100px;
    z-index: 1030;
    background: #fff;
`
const HiddenOnMobile = styled.span`
  @media screen and (max-width: 450px){
    display: none;
  }
`

export default class Navigation extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false
    }
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }
  render() {
    const { appId, numberOfCards } = this.props
    if (appId === 'clubmedcancun') {
      return clubMedNav()
    }
    if (appId === 'lokncharge') {
      return lockNchargeNav()
    }
    if (appId === 'zombiesMTL') {
      return zombiesMTLNav()
    }
    if (appId === 'lacmegantic') {
      return LacMegNav()
    }
    if (appId === 'mtl' || appId === 'demo7') {
      return MtlNav()
    }
    return (
      <Header>
        <Navbar color="faded" light expand="md">
          <NavbarBrand href={appId === 'UNDEFINED' ? '/' : `/${appId}/`} >
            <img src={logo} alt="MySmartJourney" style={{ width: '60px', height: '60px', marginRight: '8px' }} />
            <HiddenOnMobile>MySmartJourney</HiddenOnMobile>
          </NavbarBrand>
          { appId === 'UNDEFINED' && (
          <Nav className="ml-auto">
            <NavItem>
              <NavLink href="mailto:lopez@mysmartjourney.co" className="btn btn-primary" active> J'embarque!</NavLink>
            </NavItem>
          </Nav>
          )}
          { appId !== 'UNDEFINED' && (
            <div>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                    <NavLink href={routes.LANDING}>Accueil</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href={`/${appId}/`}>Voir la quête au complet</NavLink>
                  </NavItem>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      Voir une station
                    </DropdownToggle>
                    <DropdownMenu >
                      {[...Array(numberOfCards - 1)].map((e, i) => (
                        <DropdownItem href={`/${appId}/${i + 1}`} key={`navitem-${i + 1}`}>
                          Station {i + 1}
                        </DropdownItem>
                      ))}
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
              </Collapse>
            </div>
          )}
        </Navbar>
      </Header>
    )
  }
}

Navigation.defaultProps = {
  appId: 'UNDEFINED',
  numberOfCards: 0,
}
Navigation.propTypes = {
  appId: PropTypes.string,
  numberOfCards: PropTypes.number,
}
