import React from 'react';
import styled from 'styled-components'
import { Button } from 'reactstrap'
import PropTypes from 'prop-types';

import { styles } from '../../styles-variables';

const base = styled(Button)`
    border: none;
    color: ${styles.color.charcoal}
    display: block;
    font-size: ${styles.fontSize.regular};
    line-height: ${styles.lineHeight.regular};
    height: auto;
    margin: 0;
    padding: 0;
    width: 100%;
    white-space: normal;
    :hover, :active, :focus {
      color: ${styles.color.black}
    }
`;

// BUTTON or Acne
const Exploration = base.extend`
  background: ${props => props.save ? styles.color.green : styles.color.yellow};
  border-radius: ${styles.borderRadius.round};
  box-shadow: 0 0 .5rem rgba(0, 0, 0, .2);
  color: ${props => props.save ? styles.color.white : styles.color.charcoal};
  font-size: ${styles.fontSize.large};
  padding: ${styles.padding.regular};
  text-transform: ${props => props.save ? 'uppercase' : 'normal'};;
  :hover, :active, :focus {
    background: ${props => props.save ? styles.color.green : styles.color.yellow} !important;
    color: ${props => props.save ? styles.color.white : styles.color.black} !important;
  }
`;

const Application = base.extend`
  background: ${props => props.save ? styles.color.green : styles.color.blue};
  border-radius: ${styles.borderRadius.btn};
  color: ${styles.color.white}
  padding: ${styles.padding.small};
  :hover, :active, :focus {
    background: ${props => props.save ? styles.color.green : styles.color.blue};
    color: ${styles.color.white}
  }
`;

const Disabled = base.extend`
  background: ${styles.color.gray};
  border-radius: ${styles.borderRadius.btn};
  padding: ${styles.padding.regular};
  :hover, :active, :focus {
    background: ${styles.color.gray};
    color: ${styles.color.charcoal}
  }
`;

const Transparent = base.extend`
  background-color: ${styles.color.transparent};
  border-radius: ${styles.borderRadius.btn};
  border: none;
  padding: ${styles.padding.small};
  :hover, :active, :focus {
    background-color: ${styles.color.transparent};
    color: ${styles.color.black}
  }
`;


// LINK or Zelda
const Link = styled.a`
  color: ${styles.color.charcoal}
  display: inline-block;
  font-size: ${styles.fontSize.regular};
  :hover, :active, :focus {
    color: ${styles.color.black}
  }
`;

const Btn = ({
  save,
  disabled,
  children,
  app,
  transparent,
  onClick,
  link,
  href,
  className,
  id,
}) => {
  if (disabled) {
    return (<Disabled id={id} className={className} disabled>{children}</Disabled>)
  } else if (save) {
    return (<Exploration save id={id} className={className} href={href} onClick={onClick}>{children}</Exploration>)
  } else if (transparent) {
    return (<Transparent id={id} className={className} href={href} onClick={onClick}>{children}</Transparent>)
  } else if (link) {
    return (<Link id={id} className={className} href={href} onClick={onClick}>{children}</Link>)
  }

  const StyledButton = app ? Application : Exploration
  return (<StyledButton id={id} className={className}  href={href}onClick={onClick}>{children}</StyledButton>)
}

Btn.propTypes = {
  disabled: PropTypes.bool,
  app: PropTypes.bool,
  children: PropTypes.any,
  onClick: PropTypes.func,
  className: PropTypes.string,
  id: PropTypes.string,
  href: PropTypes.string,
}

export default Btn
