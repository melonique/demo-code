import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup } from 'reactstrap'


const PartVideoYoutubeForm = ({ handleOnChange, partIndex, url }) => (
  <FormGroup className="button">
    <Label htmlFor="url">Video YouTube</Label>
    <Input
      type="text"
      name={`text-${partIndex}`}
      id="url"
      value={url}
      placeholder=""
      onChange={e => (handleOnChange(e, partIndex))}
    />
  </FormGroup>
)

PartVideoYoutubeForm.defaultProps = {
  url: ''
}
PartVideoYoutubeForm.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired,
  url: PropTypes.string
}

export default PartVideoYoutubeForm
