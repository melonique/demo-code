import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { CardBody, Button } from 'reactstrap'

import { tokenIsLocked , appIsLocked } from '../../constants/lockedAppsAndTokens'



class CardContent extends Component {
  constructor(props) {
    super()
  }
  render() {
    const {
      appId,
      tokenId
    } = this.props

    return (
      <CardBody style={{ background: '#f4f4f4', paddingTop: '1.25rem' }}>
        { !tokenIsLocked(tokenId) && !appIsLocked(appId) &&
          <div>
            <Button style={{ float: 'left' }} color="blue" href={`/${appId}`}>Voir toutes</Button>
            <Button style={{ float: 'right' }} color="green" href={`/${appId}/${tokenId}/edit`}>Modifier</Button>
          </div>
         }
      </CardBody>
    )
  }
}

CardContent.defaultProps = {
  appId: 'demo1',
  tokenId : 1,
}

CardContent.propTypes = {
  appId: PropTypes.string.isRequired,
  tokenId: PropTypes.any.isRequired
}
export default (CardContent)
