import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import YouTubePlayer from 'react-player/lib/players/YouTube'
import { Player } from 'video-react'
import { videoIsAutoplay } from '../../constants/video-parameters'


import {
  CardText,
  CardBody,
  CardTitle,
  Button,
} from 'reactstrap'

import { Pastille, Img } from '../'

const SpanBlock = styled.span`
  display: block;
`
const StyledCardBody = styled(CardBody)`
  + .card-body{
    padding-top: 1.25rem;

  }
`
const CardTitleStyled = styled(CardTitle)`
  &.card-title{
    font-size: 1.5rem;
    color: #444;
    margin: 0;
    display: table-cell;
  }
`
const ResponsivePlayer = styled.div`
  position: relative;
  padding-top: 56.25%;
  .react-player {
    position: absolute;
    top: 0;
    left: 0;
    width: 100% !important;
    height: 100% !important;
  }
`

class CardContent extends Component {
  render() {
    const {
      currentCard,
      isAllCards,
      appId,
      //  error,
    } = this.props

    return (
      <div>
        <StyledCardBody style={{ background: '#f4f4f4' }}>
          <table>
            <tbody>
              <tr>
                <td><Pastille currentCardTokenId={currentCard.tokenId} color="#cc0000" /></td>
                <td style={{ paddingLeft: '1.25rem', paddingRight: '1.25rem' }}><CardTitleStyled>{currentCard.title}</CardTitleStyled></td>
              </tr>
            </tbody>
          </table>
        </StyledCardBody>

        {currentCard.parts.map((part, i) => {
          switch (part.type) {
            case 'Button':
                return (
                  <StyledCardBody key={`part-${i}`}>
                    <Button color="yellow" block href={part.url}>{part.text}</Button>
                  </StyledCardBody>
                )
            case 'Text':
              if (part.text) {
                return (
                  <StyledCardBody key={`part-${i}`}>
                    <CardText>
                      {part.text.split('\n').map((v, j) => {
                        return <SpanBlock key={`part-${i}-txt-${j}`}>{v}</SpanBlock>
                      })}
                    </CardText>
                  </StyledCardBody>
                )
              }
              return null
            case 'Picture':
              if (part.link && part.url && part.url !== '') {
                return (
                  <a href={part.link}>
                    <Img key={`part-${i}`} src={(part.base64 && part.base64 !== '' ? part.base64 : part.url)} alt="" />
                  </a>
                )
              }
              return (
                <div>
                  {part.url && part.url !== '' &&
                    <Img key={`part-${i}`} src={(part.base64 && part.base64 !== '' ? part.base64 : part.url)} alt="" />
                  }
                </div>
              )
            case 'PictureUrl':
                return (
                  <Img key={`part-${i}`} src={part.url} alt=""/>
                )
            case 'Video':
                return (
                  <Player
                    key={`part-${i}`}
                    playsInline
                    poster=""
                    preload={isAllCards ? 'metadata' : 'auto'}
                    src={part.url}
                  />
              )
            case 'VideoYoutube':
            console.log((!isAllCards && videoIsAutoplay(appId)))
            console.log(appId)
              return (
                <ResponsivePlayer key={`part-div-${i}`}>
                  <YouTubePlayer
                    className='react-player'
                    url={`https://www.youtube.com/watch?v=${part.id}`}
                    playing={(!isAllCards && videoIsAutoplay(appId))}
                    controls
                  />
                </ResponsivePlayer>

              )
            case 'Redirect':
              if (!isAllCards) {
                setTimeout(() => {
                  window.location = part.url
                }, 500)
              }
              return <span key={`part-${i}`}>Redirection en cours... </span>
            case 'Audio':
              return (
                <CardBody key={`part-${i}`}>
                  <audio controls style={{ width: '100%' }}>
                    <source
                      src={part.url}
                      type="audio/mpeg"
                    />
                    Your browser does not support the audio element. Contact us at magie@mysmartjourney.co if you need help with that.
                  </audio>
                </CardBody>
              )
            default:
              return <span key={`part-${i}`} />
          }
        })}
      </div>
    )
  }
}

CardContent.defaultProps = {
  currentCard: { parts: [{ type: '' }] },
  isAllCards: false,
}

CardContent.propTypes = {
  currentCard: PropTypes.object,
  isAllCards: PropTypes.bool
}
export default (CardContent)
