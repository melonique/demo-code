import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { connect } from 'react-redux'
import YouTube from 'react-youtube'
import { Player } from 'video-react'
import Pastille from '../Pastille'
import Btn from '../Button'
import imgPastille from '../../assets/svg/pastille.svg'

import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
  Row,
  Col
} from 'reactstrap'

import { ONE_CARD_REQUEST } from '../../constants/action-types'


const StyledCard = styled(Card)`
  max-width: 400px;
  margin: 0 auto;
`
const StyledCardBody = styled(CardBody)`
  background-color: #f4f4f4;
  margin-bottom: 1em;
  + .card-body{
    padding-top:0;
  }
`
const CardItem = styled.div`
  border-radius: 3px;
  padding: 0.25em 1em;
  margin: 1em;
  background: transparent;
  color: #333;
  border: 2px solid #ccc;

  :hover, :active, :focus {
    background: #f6f6f6;
    border-color: #007bff;
  }

  ${props => props.blue && css`
  border: 2px solid #007bff;

  :hover, :active, :focus {
    background: #dc3545;
    border-color: #dc3545;
    color: white;
  }
  `}
`;

const CenterRow = styled(Row)`
  align-items: center;
`

const CardTitleStyled = styled(CardTitle)`
  font-size: 1.5rem;
  color: #444;
`

class CardContent extends Component {
  constructor(props) {
    super()
    this.onRequestApi = props.onRequestApi.bind(this)
  }
  componentDidMount() {
    const { appId, tokenId } = this.props
    this.onRequestApi(appId, tokenId)
    this.forceUpdate()
  }
  componentWillReceiveProps() {
    if (!this.props.fetching) {
      const { appId, tokenId } = this.props
      this.onRequestApi(appId, tokenId)
      this.forceUpdate()
    }
  }
  render() {
    const {
      fetching,
      currentCard,
      //  error,
      tokenId,
      appId
    } = this.props

    return (
      <div className="App">
        {fetching ? (<div>Loading animation</div> ) : (
          (currentCard &&
            <StyledCard>
              <StyledCardBody className="title">
                <CenterRow>
                  <Col xs="3">
                    <Pastille currentCardTokenId={currentCard.tokenId} color="#cc0000" />
                  </Col>
                  <Col xs="9">
                    <CardTitleStyled>
                    {currentCard.title}
                    </CardTitleStyled>
                  </Col>
                </CenterRow>
              </StyledCardBody>
              {currentCard.parts.map((part, i) => {
                switch (part.type) {
                  case 'Button':
                      return (
                        <Btn key={`part-${i}`} href={part.url}>{part.text}</Btn>
                      )
                  case 'Text':
                      return (
                        <StyledCardBody key={`part-${i}`}>
                          <CardText>
                            {part.text}
                          </CardText>
                        </StyledCardBody>
                      )
                  case 'Picture':
                  case 'PictureUrl':
                      return (
                        <CardImg key={`part-${i}`} top width="100%" src={part.url} />
                      )
                  case 'Video':
                      return (
                        <Player
                          key={`part-${i}`}
                          playsInline
                          poster="https://video-react.js.org/assets/poster.png"
                          src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                        />
                      )
                  case 'VideoYoutube':
                    return (
                      <YouTube
                        key={`part-${i}`}
                        opts={{ width: '100%', height: 'auto' }}
                        videoId="2g811Eo7K8U"
                      />
                    )
                  case 'Audio':
                      return <span key={`part-${i}`}>This is {part.type}</span>
                  default:
                    return <span />
                }
              })}

              <StyledCardBody className="link">
                <Btn link href="http://www.google.com/">Link a href</Btn>
                <hr />

                <hr />
                <Btn>Button avec Icon <img src={imgPastille} width="32" height="32" /></Btn>
                <hr />
                <Btn app>Button Application</Btn>
                <hr />
                <Btn transparent>Button transparent</Btn>
                <hr />

              </StyledCardBody>

              <CardItem>{JSON.stringify(currentCard)}</CardItem>
              <StyledCardBody className="controls">
                <hr />
                <Button style={{ float: 'right' }} color="blue" href={`/${appId}/${tokenId}/edit`}>Modifier</Button>
              </StyledCardBody>
            </StyledCard>
            ))}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  fetching: state.root.fetching,
  error: state.root.error,
  currentCard: state.root.currentCard,
  allCards: state.root.allCards,
})

const mapDispatchToProps = dispatch => ({
  onRequestApi: (appId, tokenId) => dispatch({ type: ONE_CARD_REQUEST, appId, tokenId })
})

CardContent.defaultProps = {
  currentCard: {parts:[ {type: 'base'} ]},
  fetching: false
}

CardContent.propTypes = {
  currentCard : PropTypes.object,
  fetching: PropTypes.bool,
}
export default connect(mapStateToProps, mapDispatchToProps)(CardContent)
