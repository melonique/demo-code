import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup } from 'reactstrap'

const PartPictureForm = ({ handlePictureChange, handleOnChange, partIndex, url, isCompressed }) => (
  <div>
    <FormGroup>
      <Label htmlFor={`file-${partIndex}`}>Téléverser une image</Label>
      <Input
        type="file"
        accept="image/*"
        name={`file-${partIndex}`}
        id="file"
        onChange={e => (handlePictureChange(e, partIndex))}
      />
    </FormGroup>
    <FormGroup>
      <Label check>
        <Input type="checkbox" onChange={e => (handleOnChange(e, partIndex))} id="isCompressed" defaultChecked={isCompressed} /> Compresser l'image (recommandé)
      </Label>
    </FormGroup>
  </div>
)

PartPictureForm.propTypes = {
  url: PropTypes.any,
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired,
  handlePictureChange: PropTypes.func.isRequired,
}

export default PartPictureForm
