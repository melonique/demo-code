import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup } from 'reactstrap'


const PartTextForm = ({ handleOnChange, partIndex, text }) => (
  <FormGroup>
    <Label htmlFor={`text-${partIndex}`}>Texte</Label>
    <Input
      type="textarea"
      name={`text-${partIndex}`}
      id="text"
      value={text}
      onChange={e => (handleOnChange(e, partIndex))}
    />
  </FormGroup>
)


PartTextForm.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired
}

export default PartTextForm
