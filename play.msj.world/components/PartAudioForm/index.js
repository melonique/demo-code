import React from 'react'
import PropTypes from 'prop-types'
import { Label, Input, FormGroup, FormText } from 'reactstrap'

const PartAudioForm = ({ handleOnChange, partIndex, url }) => (
  <FormGroup>
    <Label htmlFor={`url-${partIndex}`}>Téléverser un fichier audio</Label>
    <Input
      type="file"
      name={`file-${partIndex}`}
      id="file"
      accept="audio/mpeg"
      onChange={e => (handleOnChange(e, partIndex))}
    />
    <FormText color="muted">
      format: .mp3, .mpeg
    </FormText>
  </FormGroup>
)

PartAudioForm.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  partIndex: PropTypes.number.isRequired
}

export default PartAudioForm
