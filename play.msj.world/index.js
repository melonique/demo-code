// react
import React from 'react'
import ReactDOM from 'react-dom'
import { injectGlobal } from 'styled-components'
//  redux saga
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { Provider } from 'react-redux'
// history
import { routerReducer, routerMiddleware } from 'react-router-redux'

// video.js
import 'video-react/dist/video-react.css'

import LinotteRegular from './assets/fonts/Linotte/LinotteRegular.otf'
import LinotteSemiBold from './assets/fonts/Linotte/LinotteSemiBold.otf'
import LinotteBold from './assets/fonts/Linotte/LinotteBold.otf'

// Logger with default options
// import logger from 'redux-logger'

// local data for redux saga
import { rootReducer } from './services/reducers/'
import watcherSaga from './services/workers'
import history from './services/history'

import { App } from './containers'
import registerServiceWorker from './registerServiceWorker'

import './bootstrap.css'
import './index.css'
import './zombiesMTL.css'
// create the saga middleware
const sagaMiddleware = createSagaMiddleware()

// Build the middleware for intercepting and dispatching navigation actions
const historyMiddleware = routerMiddleware(history)

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose

// LOGGER / DEBUGER CONSOLE ICI
const enhancer = composeEnhancers(
  // applyMiddleware(sagaMiddleware,historyMiddleware,  logger)
  applyMiddleware(historyMiddleware, sagaMiddleware)
)

// create a redux store with our reducer above and middleware
const store = createStore(
  combineReducers({
    root: rootReducer,
    router: routerReducer
  }),
  enhancer
)

// run the saga
sagaMiddleware.run(watcherSaga)

injectGlobal`
    @font-face {
        font-family: 'Linotte';
        src: url(${LinotteRegular}) format('opentype');
        font-weight: light;
        font-style: normal;
    }

    @font-face {
        font-family: 'Linotte';
        src: url(${LinotteBold}) format('opentype');
        font-weight: bold;
        font-style: normal;
    }

    @font-face {
        font-family: 'Linotte';
        src: url(${LinotteSemiBold}) format('opentype');
        font-weight: normal;
        font-style: normal;
    }

    * {
        font-family: 'Linotte', sans-serif;
    }
`;


ReactDOM.render(<Provider store={store}><App history={history} /></Provider>, document.getElementById('root'))

registerServiceWorker()
