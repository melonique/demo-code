/*
Jaune : #f9be00
Rose = #ff3065
Rouge = #d1003b
Vert = #2ab56a
Bleu = #3370ec
*/

export const styles = {
  color: {
    blue: '#3370ec',
    red: '#d1003b',
    green: '#2ab56a',
    yellow: '#f9be00',
    transparent: 'transparent',
    white: '#fff',
    gray: '#ccc',
    black: '#000',
    charcoal: '#333',
    pink: '#ff3065'
  },
  borderRadius: {
    btn: '.25em',
    round: '25em'
  },
  fontSize: {
    small: '0.8rem',
    regular: '1rem',
    medium: '1.25rem',
    large: '1.5rem'
  },
  padding: {
    small: '7px 18px',
    regular: '14px 36px',
    large: '21px 54px'
  },
  lineHeight: {
    small: '.8em',
    regular: '1em',
    large: '1,25em'
  }
}

export default styles
