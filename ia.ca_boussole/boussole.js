__cw__define(["jquery", "boussole.navigation", "boussole.controller", "analytics_ia", "jquery.validate", "loadingGif"], function ($, nav, Controller, ANA) {
    "use strict";

    var formBoussole = $("#form-boussole"),
        fname = formBoussole.attr("data-utag-fname");

    //    ANA.setDev(true);


    //Fonction init

    var init = function init(data, textes) {
        nav.init();
        Controller.init('boussole-wrapper', data, textes);

        $('#sendParEmail').click(function () {
            $('#formSendEmail').slideToggle();
            $('#txtCourriel').val('');
        });

        $('#resultats-boussole').on('click', '.open-close', function () {
            var $this = $(this);
            $this.prev('ul').toggleClass('closed');
            $this.toggleClass('est-ferme est-ouvert');
        });
    };

    /** --- validationdu form envoi PDF ***/
    $('#formSendEmail').validate({
        success: function success(label, element) {
            label.remove();
        },
        errorElement: "label",
        errorPlacement: function errorPlacement(error, element) {
            error.attr("id", $(element).attr("name") + "_error");
            error.attr("class", "error info-erreur icone-exclamation-formulaire");

            if (element.attr('data-msg-target')) {
                var target = element.attr('data-msg-target');
                if (error.text() != '') {
                    error.appendTo($(target));
                }
                return;
            }
            error.insertAfter(element.parent().children().last());
        },
        invalidHandler: function invalidHandler() {
            ANA.ia_utag_form($('#formSendEmail').attr('data-utag-fname'), 'error_on_submit');
        },
        submitHandler: function submitHandler(form, event) {
            event.preventDefault();
            ANA.ia_utag_form($('#formSendEmail').attr('data-utag-fname'), 'submit');
            ajaxPDF();
            return false;
        }
    });

    var ajaxPDF = function ajaxPDF() {
        $('#msgEnvoiErreur, #msgEnvoiSuccess').hide();
        var data = JSON.parse($('#dataPDFPRINT').val());

        //$('#sendParEmail').click();


        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                if (data[property].constructor === Array) {
                    if (data[property].length < 1) {
                        data[property] = null;
                    }
                }
                if (data[property].constructor === String) {
                    if (data[property] == '') {
                        data[property] = null;
                    }
                }
            }
        }

        var dataBota = {
            textes: btoa(encodeURIComponent(JSON.stringify(data.textes))),
            situation_produits: btoa(encodeURIComponent(JSON.stringify(data.situation_produits))),
            situation_outils: btoa(encodeURIComponent(JSON.stringify(data.situation_outils))),
            situation_conseils: btoa(encodeURIComponent(JSON.stringify(data.situation_conseils))),
            epargne_produits: btoa(encodeURIComponent(JSON.stringify(data.epargne_produits))),
            epargne_outils: btoa(encodeURIComponent(JSON.stringify(data.epargne_outils))),
            epargne_conseils: btoa(encodeURIComponent(JSON.stringify(data.epargne_conseils))),
            biens_produits: btoa(encodeURIComponent(JSON.stringify(data.biens_produits))),
            biens_outils: btoa(encodeURIComponent(JSON.stringify(data.biens_outils))),
            biens_conseils: btoa(encodeURIComponent(JSON.stringify(data.biens_conseils))),
            courriel: $('#courriel').val()
        };

        $.ajax({
            url: "/IA_API/FormulaireBoussoleController/EnvoyerResultatBoussole",
            //   url: "/IA_API/FormulaireBoussoleController/TelechargerResultatBoussole",
            dataType: "json",
            data: dataBota,
            type: "POST",
            statusCode: {
                404: function _() {
                    alert("page not found");
                }
            },
            beforeSend: function beforeSend() {
                $('#formSendEmail .btn-trouver').startLoading();
            },
            success: function success(output) {
                if (JSON.parse(output).courrielEnvoie == true) {
                    $('#formSendEmail #courriel').val('');
                    $('#msgEnvoiSuccess').show();
                } else {
                    $('#msgEnvoiErreur').show();
                }
            },
            error: function error(XMLHttpRequest, textStatus, errorThrown) {
                //stuff to do on error
                console.log(XMLHttpRequest);
                $('#msgEnvoiErreur').show();
            },
            complete: function complete() {
                $('#formSendEmail .btn-trouver').stopLoading();
            } //stuff to when complete after error or succes
        });
    };

    /*-----------------------------Gestion de la validation et submit------------------------------*/

    formBoussole.validate({
        success: function success(label, element) {
            label.remove();
        },

        errorElement: 'label',
        errorPlacement: function errorPlacement(error, element) {
            error.attr("id", $(element).attr("name") + "_error");
            error.attr("class", "error info-erreur icone-exclamation-formulaire");

            if (element.attr('data-msg-target')) {
                var target = element.attr('data-msg-target');
                if (error.text() != '') {
                    error.appendTo($(target));
                }
                return;
            }
            error.insertAfter(element.parent().children().last());
        },
        invalidHandler: function invalidHandler() {
            ANA.ia_utag_form(fname + location.hash, 'error_on_submit');
        },
        submitHandler: function submitHandler(form, event) {
            event.preventDefault();
            ANA.ia_utag_form(fname + location.hash, 'submit');
            return false;
        }
    });

    return {
        init: init
    };
});

//Polyfill de Array.from  utilis� pour le reduce 
Array.from || (Array.from = function () {
    var r = Object.prototype.toString,
        n = function n(_n) {
        return "function" == typeof _n || "[object Function]" === r.call(_n);
    },
        t = function t(r) {
        var n = Number(r);return isNaN(n) ? 0 : 0 !== n && isFinite(n) ? (n > 0 ? 1 : -1) * Math.floor(Math.abs(n)) : n;
    },
        e = Math.pow(2, 53) - 1,
        o = function o(r) {
        var n = t(r);return Math.min(Math.max(n, 0), e);
    };return function (r) {
        var t = this,
            e = Object(r);if (null == r) throw new TypeError("Array.from requires an array-like object - not null or undefined");var a,
            i = arguments.length > 1 ? arguments[1] : void 0;if (void 0 !== i) {
            if (!n(i)) throw new TypeError("Array.from: when provided, the second argument must be a function");arguments.length > 2 && (a = arguments[2]);
        }for (var u, f = o(e.length), c = n(t) ? Object(new t(f)) : new Array(f), h = 0; h < f;) {
            u = e[h], c[h] = i ? void 0 === a ? i(u, h) : i.call(a, u, h) : u, h += 1;
        }return c.length = f, c;
    };
}());