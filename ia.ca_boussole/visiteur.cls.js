__cw__define(['knockout'], function (ko) {

    var Visiteur = function Visiteur() {
        var self = this;
        /**
         * change les autres parametres pour false si la nouvelle valeur est true
         * @param  {int}    nv     nouvelle valeur
         * @param  {...array} autres toutes les autres valeurs a changer mis en array
         * @return {fuckall}
         */

        var makeRadio = function makeRadio(nv) {
            for (var _len = arguments.length, autres = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                autres[_key - 1] = arguments[_key];
            }

            autres.forEach(function (current) {
                if (nv == 1) {
                    current(0);
                }
            });
        };

        self.situation = ko.observable({
            "celibataire": ko.observable(),
            "couple": ko.observable()
        });
        //make radio out of tige
        self.situation().celibataire.subscribe(function (nv) {
            makeRadio(nv, self.situation().couple);
        });
        self.situation().couple.subscribe(function (nv) {
            makeRadio(nv, self.situation().celibataire);
        });

        self.enfants = ko.observable({
            "non": ko.observable(),
            "nonPasEncore": ko.observable(),
            "moin12mois": ko.observable(),
            "de1a5ans": ko.observable(),
            "plus5ans": ko.observable(),
            "enAttend": ko.observable()
        });
        self.enfants().plusieurs = ko.computed(function () {
            var nbr = 0;
            if (self.enfants().moin12mois()) {
                nbr++;
            }
            if (self.enfants().de1a5ans()) {
                nbr++;
            }
            if (self.enfants().plus5ans()) {
                nbr++;
            }
            if (self.enfants().enAttend()) {
                nbr++;
            }
            return nbr > 1;
        }, self);

        self.enfants().non.subscribe(function (nv) {
            makeRadio(nv, self.enfants().nonPasEncore, self.enfants().moin12mois, self.enfants().de1a5ans, self.enfants().plus5ans, self.enfants().enAttend);
        });
        self.enfants().nonPasEncore.subscribe(function (nv) {
            makeRadio(nv, self.enfants().non, self.enfants().moin12mois, self.enfants().de1a5ans, self.enfants().plus5ans, self.enfants().enAttend);
        });
        self.enfants().moin12mois.subscribe(function (nv) {
            makeRadio(nv, self.enfants().non, self.enfants().nonPasEncore);
        });
        self.enfants().de1a5ans.subscribe(function (nv) {
            makeRadio(nv, self.enfants().non, self.enfants().nonPasEncore);
        });
        self.enfants().plus5ans.subscribe(function (nv) {
            makeRadio(nv, self.enfants().non, self.enfants().nonPasEncore);
        });
        self.enfants().enAttend.subscribe(function (nv) {
            makeRadio(nv, self.enfants().non, self.enfants().nonPasEncore);
        });

        self.habitation = ko.observable({
            "locataire": ko.observable(),
            "futurProprietaire": ko.observable(),
            "proprietaire": ko.observable()
        });
        self.habitation().locataire.subscribe(function (nv) {
            makeRadio(nv, self.habitation().futurProprietaire, self.habitation().proprietaire);
        });
        self.habitation().futurProprietaire.subscribe(function (nv) {
            makeRadio(nv, self.habitation().locataire, self.habitation().proprietaire);
        });
        self.habitation().proprietaire.subscribe(function (nv) {
            makeRadio(nv, self.habitation().futurProprietaire, self.habitation().locataire);
        });

        self.travail = ko.observable({
            "autonome": ko.observable(),
            "entrepreneur": ko.observable(),
            "salaire": ko.observable(),
            "retraite": ko.observable(),
            "etudiant": ko.observable(),
            "maison": ko.observable()
        });

        self.transport = ko.observable({
            "auto": ko.observable(),
            "moto": ko.observable(),
            "vr": ko.observable(),
            "velo": ko.observable(),
            "transportCommun": ko.observable(),
            "souliers": ko.observable()
        });

        self.projets = ko.observable({
            "maison": ko.observable(),
            "chalet": ko.observable(),
            "voyage": ko.observable(),
            "renovations": ko.observable(),
            "mariage": ko.observable(),
            "sabatique": ko.observable(),
            "etudes": ko.observable(),
            "entreprise": ko.observable(),
            "neSaitPas": ko.observable()
        });

        self.retraite = ko.observable({
            "cygale": ko.observable(),
            "fourmis": ko.observable()
        });
        self.retraite().cygale.subscribe(function (nv) {
            makeRadio(nv, self.retraite().fourmis);
        });
        self.retraite().fourmis.subscribe(function (nv) {
            makeRadio(nv, self.retraite().cygale);
        });

        self.fillItUp = function () {
            self.situation().celibataire(1);

            self.enfants().moin12mois(1);
            //   self.enfants().de1a5ans(1);
            // self.enfants().plus5ans(1);
            self.enfants().enAttend(1);
            self.habitation().proprietaire(1);
            //   self.travail().autonome(1);
            //    self.travail().entrepreneur(1);
            self.travail().salaire(1);
            //    self.travail().retraite(1);
            //    self.travail().maison(1);
            //    self.travail().etudiant(1);
            self.transport().auto(1);
            //     self.transport().moto(1);
            //   self.transport().vr(1);
            //   self.transport().velo(1);
            //   self.transport().transportCommun(1);
            //     self.transport().souliers(1);
            self.projets().maison(1);
            self.projets().chalet(1);
            self.projets().voyage(1);
            self.projets().renovations(1);
            self.projets().mariage(1);
            self.projets().sabatique(1);
            self.projets().etudes(1);
            self.projets().entreprise(1);
            self.projets().neSaitPas(1);
            self.retraite().fourmis(1);
        };
    };

    return {
        Visiteur: Visiteur
    };
});