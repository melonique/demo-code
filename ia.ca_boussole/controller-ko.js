__cw__define(['knockout', 'boussole.visiteur.cls', 'boussole.generation-titres', 'knockout-extenders'], function (ko, CLS, gTitres) {
    "use strict";

    /**
     * J'ai le cerveau ramolit, dsl pour le 💩 code
     */

    var _VM = function _VM(optData) {
        // 🔨⏰ Can't touch me
        Object.freeze(optData);
        var self = this;
        self.visiteur = ko.observable(new CLS.Visiteur());

        self.restart = function () {
            $(document).scrollTop(0);
            location.reload();
        };

        /**
         * on renvoi l'objet visiteur mais seulement les propriétés a true
         * @return {object} 
         **/
        self.currentJSVrai = ko.computed(function () {
            var complete = ko.toJS(self.visiteur()),
                vraiSeulement = {};
            for (var question in complete) {
                if (complete.hasOwnProperty(question)) {
                    var reponses = complete[question];
                    for (var reponse in reponses) {
                        if (reponses.hasOwnProperty(reponse)) {
                            var resultat = reponses[reponse];
                            if (resultat) {
                                if (!vraiSeulement[question]) {
                                    // on evite un undefined
                                    vraiSeulement[question] = {};
                                }
                                vraiSeulement[question][reponse] = resultat;
                            }
                        }
                    }
                }
            }
            return JSON.stringify(vraiSeulement);
        }, self);

        var currentJs = function currentJs() {
            var currentJsData = ko.toJS(self.visiteur());

            return currentJsData;
        };

        /**
        * test les conditions de tous les items réponses pour valider si il a au moins une des conditions de remplies
        * et vérifie si singulier et plurier aussi
        * @return {[type]} [description]
        */
        self.testConditions = function (categorie) {
            var isOneTrue = void 0,
                itemsReponseAAfficher = [],
                clefs = [];
            //roule dans les objets réponses
            optData.forEach(function (element) {

                var conditions = element.conditions,
                    maCle = void 0;
                isOneTrue = false;
                element.texte = ko.observable();

                //on test les conditions
                for (var groupe in conditions) {
                    if (conditions.hasOwnProperty(groupe)) {
                        var specific = conditions[groupe];
                        for (var key in specific) {
                            if (specific.hasOwnProperty(key)) {
                                var value = specific[key];
                                if (self.visiteur()[groupe]()[key]() == value) {
                                    //valide si nos conditions sont == a notre situaiton de visiteur
                                    isOneTrue = true;
                                }
                            }
                        }
                    }
                } //for eeach condition

                maCle = element.cle != '' ? element.type + element.categorie + element.cle : null;

                if (maCle && clefs.indexOf(maCle) < 0 && isOneTrue) {
                    //si j'ai une clef et que elle a pas été tuilisé
                    clefs.push(maCle);
                } else if (maCle) {
                    //si j'ai une key mais quelle est dej dnas larray, on met isOneTrue pour pas qu'on l'ajoute aux item de réponse
                    isOneTrue = false;
                }
                if (isOneTrue && element.categorie === categorie) {
                    // si une condition est bonne ET que le pluriel est bon, on l'ajoute a l'array des reponses OK ET que on en a pas d'autre d ela meme clef
                    itemsReponseAAfficher.push(element);
                    clefs.push(element.clef);
                }
            }); //for each item

            return itemsReponseAAfficher; //on retourne l'array de réponses OK
        };

        self.TitreProduitsSituation = ko.computed(function () {
            return gTitres.getTitreProduitsSituation(currentJs());
        }, self);
        self.produitsSituaton = ko.computed(function () {
            return self.testConditions("situation-personelle");
        }, self);

        self.TitreProduitsBiens = ko.computed(function () {
            return gTitres.getTitreProduitsBiens(currentJs());
        }, self);
        self.produitsBiens = ko.computed(function () {
            return self.testConditions("biens");
        }, self);

        self.TitreProduitsEpargne = ko.computed(function () {
            return gTitres.getTitreProduitsEpargne(currentJs());
        }, self);
        self.produitsEpargne = ko.computed(function () {
            return self.testConditions("epargne");
        }, self);

        self.filterByType = function (array, recherche) {
            return array.filter(function (produit) {
                return produit.type == recherche;
            });
        };

        self.fillAndJump = function () {
            self.visiteur().fillItUp();
            location.hash = "#r";
            console.log('optData', optData);
            console.log('data visiteur', ko.toJS(self.visiteur()));
        };

        self.logOptData = function () {
            return console.log('optData', JSON.stringify(optData));
        };
        self.logUserData = function () {
            return console.log('userData', ko.toJSON(self.visiteur()));
        };

        self.dataPDF = ko.computed(function () {

            var reduceFnct = function reduceFnct(prev, item) {
                return [].concat(prev, [item.id]);
            };

            var produitsSituation = ko.toJS(self.filterByType(self.produitsSituaton(), 'produit')).reduce(reduceFnct, ''),
                outilsSituation = ko.toJS(self.filterByType(self.produitsSituaton(), 'outil')).reduce(reduceFnct, ''),
                conseilsSituation = ko.toJS(self.filterByType(self.produitsSituaton(), 'conseil')).reduce(reduceFnct, ''),
                produitsBiens = ko.toJS(self.filterByType(self.produitsBiens(), 'produit')).reduce(reduceFnct, ''),
                outilsBiens = ko.toJS(self.filterByType(self.produitsBiens(), 'outil')).reduce(reduceFnct, ''),
                conseilsBiens = ko.toJS(self.filterByType(self.produitsBiens(), 'conseil')).reduce(reduceFnct, ''),
                produitsEpargne = ko.toJS(self.filterByType(self.produitsEpargne(), 'produit')).reduce(reduceFnct, ''),
                outilsEpargne = ko.toJS(self.filterByType(self.produitsEpargne(), 'outil')).reduce(reduceFnct, ''),
                conseilsEpargne = ko.toJS(self.filterByType(self.produitsEpargne(), 'conseil')).reduce(reduceFnct, '');

            var dataUser = {
                textes: {
                    SousTitreSituation: self.TitreProduitsSituation(),
                    SousTitreEpargne: self.TitreProduitsEpargne(),
                    SousTitreBiens: self.TitreProduitsBiens()
                },
                situation_produits: produitsSituation,
                situation_outils: outilsSituation,
                situation_conseils: conseilsSituation,

                epargne_produits: produitsEpargne,
                epargne_outils: outilsEpargne,
                epargne_conseils: conseilsEpargne,

                biens_produits: produitsBiens,
                biens_outils: outilsBiens,
                biens_conseils: conseilsBiens
            };

            return dataUser;
        }, self);
    }; //VM

    var init = function init(vmID, data, textes) {
        //   alert('controller applied')
        gTitres.setTextes(textes);

        var VM = new _VM(data);
        ko.applyBindings(VM, document.getElementById(vmID));
    };

    return {
        init: init
    };
});