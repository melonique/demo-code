__cw__define(["jquery", "sammy", "matchHeight", "analytics_ia", 'pattern-personnalisation', "pageTransitions"], function ($, Sammy, matchHeight, ANA, PERSO) {
    "use strict";

    var $formBoussole = $("#form-boussole"),
        fname = $formBoussole.attr("data-utag-fname"),
        PAGE_LIST = ['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'r'],
        page_current = "q1",
        //default
    original_page_name = utag.data.page_name;

    //Fonctions pour changement de page

    //les plugins animIn et animOut s'utilise comme cela: $(selecteur).animIn(classeAnimation, vitesse-tablet-et-desktop, vitesse-mobile, callback); 

    function _pageSuivante(id) {
        _scrollTop("#header");
        var pageCourante = $(".page-active"),
            pageSuivante = $("." + id),
            pageResultats = $("#resultats-boussole");
        //backgroundCourant = $(".background-actif"),
        //backgroundSuivant = $("#bg-"+id);
        pageResultats.css("display", "none");

        if (pageSuivante.length > 0) {
            pageCourante.animOut("pt-page-moveToLeftFade", 700, 0);
            pageSuivante.animIn("pt-page-moveFromRightFade", 900, 900, function () {
                //backgroundCourant.removeClass("background-actif");
                //backgroundSuivant.addClass("background-actif");
            });
        }
    }

    function _pagePrecedente(id) {
        _scrollTop("#header");
        var pageCourante = $(".page-active"),
            pagePrecedente = $("." + id),
            pageResultats = $("#resultats-boussole");
        //backgroundCourant = $(".background-actif"),
        //backgroundPrecedent = $("#bg-" + id);
        pageResultats.css("display", "none");

        if (pagePrecedente.length > 0) {
            pageCourante.animOut("pt-page-moveToRightFade", 700, 0);
            pagePrecedente.animIn("pt-page-moveFromLeftFade", 900, 900, function () {
                //backgroundCourant.removeClass("background-actif");
                //backgroundPrecedent.addClass("background-actif");
            });
        }
    }

    function _pageResultats() {
        _scrollTop("#header");
        var pageCourante = $(".page-active"),
            pageResultats = $("#resultats-boussole");

        pageCourante.animOut("pt-page-fadeOut", 700, 0);
        pageResultats.animIn("pt-page-fadeIn", 900, 900);
    }

    //Détection support utilisateur

    function _isMobile() {
        var largeurEcran = $(window).width();
        if (largeurEcran < 768) {
            return true;
        }
    }

    //Fonction scroll to top

    function _scrollTop(option) {
        var vitesseAnimation = 500;
        if (_isMobile() == true) {
            vitesseAnimation = 0;
        }
        $("html, body").stop().animate({ scrollTop: $(option).offset().top }, vitesseAnimation);
    }

    var _saveANAdata = function _saveANAdata() {
        var step = page_current,
            data = JSON.parse($('#currentJS').val()),
            dataToSaveByPage = {
            q1: 'situation',
            q2: 'habitation',
            q3: 'enfants',
            q4: 'travail',
            q5: 'transport',
            q6: 'projets',
            q7: 'retraite'
        },
            dataKey = dataToSaveByPage[step];

        //si c'est une page a saver
        if (dataKey) {
            for (var key in data[dataKey]) {
                if (data[dataKey].hasOwnProperty(key)) {
                    var value = data[dataKey][key];
                    ANA.ia_utag_userInfo('boussole', dataKey, key);
                }
            }
        }
    };

    var _saveUserProfilePersonalisation = function _saveUserProfilePersonalisation() {
        var userAnswers = JSON.parse($('#currentJS').val());

        var profilePretHypothecaireId = '{14CF53FF-5FF7-45DE-BF5C-E14F36A0EFA4}';

        if (userAnswers.habitation.locataire) {
            PERSO.forcePattern(profilePretHypothecaireId, 'Locataire');
        }
        if (userAnswers.habitation.proprietaire) {
            PERSO.forcePattern(profilePretHypothecaireId, 'Proprietaire autre');
            PERSO.forcePattern(profilePretHypothecaireId, 'Proprietaire IA');
        }
        if (userAnswers.habitation.futurProprietaire) {
            PERSO.forcePattern(profilePretHypothecaireId, 'Acheteur');
        }
    };

    //au boot de l'app: 
    location.hash = PAGE_LIST[0];
    // gestion de # de l'url
    Sammy(function () {
        this.disable_push_state = true;
        // this.disable_push_state = true;
        //as un # de base
        this.get('#:page', function () {

            var nouvellePageIndex = PAGE_LIST.indexOf(this.params.page),
                currentPageIndex = PAGE_LIST.indexOf(page_current);
            //si existe pas out 
            if (nouvellePageIndex == -1) {
                return;
            }

            if (this.params.page == "r") {
                if ($formBoussole.valid()) {
                    _saveANAdata();
                    ANA.ia_utag_form(fname + location.hash, 'submit');
                    _pageResultats();
                } else {
                    location.hash = page_current;
                    return;
                }
            }
            //vérifier l'index de la nouvelle page par rapport a la page que je suis présetement
            // si prévcédent, GO
            if (nouvellePageIndex > currentPageIndex && this.params.page !== "r") {
                if ($formBoussole.valid()) {
                    _saveANAdata();
                    ANA.ia_utag_form(fname + location.hash, 'submit');
                    _pageSuivante(this.params.page);
                } else {
                    location.hash = page_current;
                    return;
                }
            } else if (nouvellePageIndex < currentPageIndex) {
                // si suivant : valide > go
                _pagePrecedente(this.params.page);
            }
            // toujours: update current page.;
            if (page_current != this.params.page) {
                ANA.ia_utag_view(original_page_name + "-" + this.params.page);
                page_current = this.params.page;
            }
            // si je quite la page q2 
            if (this.params.page === 'q3') {
                _saveUserProfilePersonalisation();
            }
        });
        //pas de # ou # vide
        this.get('', function () {
            //   this.app.runRoute('get', '#q1');
        });
    }).run();

    //Fonction init

    var init = function init() {

        $(".page-active").animIn("pt-page-fadeIn", 900, 900);
        $(".background-boussole").css("transition", "opacity 2.5s");
        $("#bg-q6").addClass("background-actif");

        $(".page-boussole").each(function () {
            $(this).find(".flag-matchHeight").matchHeight();
        });
    };

    return {
        init: init
    };
});