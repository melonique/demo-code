__cw__define([], function () {
    "use strict";

    /*** revealing module pattern ***/

    var textes = {};

    var setTextes = function setTextes(texte) {
        textes = texte;
    };
    var getTextes = function getTextes() {
        return textes;
    };

    var _buildAnswerArray = function _buildAnswerArray(myReponses) {
        for (var _len = arguments.length, toVerify = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            toVerify[_key - 1] = arguments[_key];
        }

        var aTitre = [];
        //    myReponses = JSON.parse(self.currentJSVrai());
        toVerify.forEach(function (question) {
            if (myReponses[question]) {
                for (var reponse in myReponses[question]) {
                    if (myReponses[question].hasOwnProperty(reponse) && myReponses[question][reponse]) {
                        var resultat = myReponses[question][reponse];
                        if (textes[question][reponse] && textes[question][reponse] !== '') {
                            aTitre.push(textes[question][reponse]);
                        }
                    }
                }
            }
        });
        return aTitre;
    };
    var _buildAnswerString = function _buildAnswerString(aTitre) {
        var str = aTitre.join(', '),
            pos = str.lastIndexOf(',');

        if (pos > 0) {
            return str.substring(0, pos) + textes.utilitaires.separateurEt + str.substring(pos + 1);
        }
        return str;
    };

    var getTitreProduitsEpargne = function getTitreProduitsEpargne(myReponses) {
        //peu importe le projet, on a juste un texte pour ca.
        var aTitre = _buildAnswerArray(myReponses, "retraite");
        return aTitre[0];
    };

    var getTitreProduitsBiens = function getTitreProduitsBiens(myReponses) {
        var aTitre1 = _buildAnswerArray(myReponses, "habitation"),
            aTitre2 = _buildAnswerArray(myReponses, "transport"),
            textePocession = aTitre2.length > 0 ? textes.utilitaires.texteTransport : '';
        return textes.utilitaires.introBiens + _buildAnswerString(aTitre1) + textePocession + _buildAnswerString(aTitre2);
    };

    var getTitreProduitsSituation = function getTitreProduitsSituation(myReponses) {
        var intoAvecEnfant = textes.utilitaires.texteAvecEnfants,

        // on affiche pas le texte d'enfants si j'ai pas d'enfants (2 cas possible) ou si j'en attend mais que j'en ai pas d'autre (donc plusieurs a false);
        texteEnfants = myReponses.enfants.non || myReponses.enfants.nonPasEncore || myReponses.enfants.enAttend && !myReponses.enfants.plusieurs ? '' : intoAvecEnfant + _buildAnswerString(_buildAnswerArray(myReponses, "enfants"));

        var texteCouple = myReponses.situation.couple ? _buildAnswerString(_buildAnswerArray(myReponses, "situation")) : '';

        return ' ' + textes.utilitaires.introSituation + ' ' + _buildAnswerString(_buildAnswerArray(myReponses, "travail")) + '\n                    ' + texteCouple + '\n                    ' + texteEnfants;
    };

    return {
        setTextes: setTextes,
        getTextes: getTextes,
        getTitreProduitsEpargne: getTitreProduitsEpargne,
        getTitreProduitsBiens: getTitreProduitsBiens,
        getTitreProduitsSituation: getTitreProduitsSituation
    };
});