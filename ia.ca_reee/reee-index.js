__cw__define(['reee-direct-controller', 'jquery', 'analytics_ia', 'active_campaign', 'reee-direct-pdf', 'bootstrap-slider', 'jquery.validate', 'jquery.validate.additionalMethods'], function (controller, $, ANA, AC) {

    // REVIEW THIS FILE PLZ

    var calculatorCount = 0;

    var DEFAULT = {
        minCotisation: 25,
        maxCotisation: 500,
        minAge: 0,
        maxAge: 17,
        maxSubFederalesAVie: 7200,
        maxSubFederalesAnnuel: 500,
        maxSubFederalesAnnuelRecuperation: 1000,
        maxCotisationAVie: 50000,
        tauxInteretPlacement: 3,
        QC: {
            maxSubProvincialesAVie: 3600,
            maxSubProvincialesAnnuel: 250,
            maxSubProvincialesAnnuelRecuperation: 500
        },
        SK: {
            maxSubProvincialesAVie: 4500,
            maxSubProvincialesAnnuel: 250,
            maxSubProvincialesAnnuelRecuperation: 500
        },
        ON: {
            maxSubProvincialesAVie: 0,
            maxSubProvincialesAnnuel: 0,
            maxSubProvincialesAnnuelRecuperation: 0
        }
    };

    var FormatCP = function FormatCP(value) {
        value = value.toUpperCase();
        if (value.substr(3, 1) != ' ') {
            value = value.splice(3, 0, ' ');
        }
        return value;
    };
    var init = function init(IDVM) {
        setupValidation();
        setupSliders(DEFAULT);
        controller.init(IDVM, DEFAULT, { setupPie: setupPie });
    };
    /**
     * Set les validations. assure que le CP est valide en le mettant dans le DOM.
     * @return {undefined}
     */
    function setupValidation() {
        var cpTimeout = null;
        var fname = $('#reee-controls').attr('data-fname');
        $('#reee-controls').validate({
            /**
            *   Wait for 1000ms before validating the input. Does not need a blur.
            **/
            onkeyup: function onkeyup(element) {
                clearTimeout(cpTimeout);
                cpTimeout = setTimeout(function () {
                    $(element).valid();
                }, 750);
            },
            onblur: function onblur(element) {},
            success: function success(label, element) {
                label.remove();
                if ($(element).attr('name') == 'CodePostal') {
                    ANA.ia_utag_form(fname, 'submit');
                    $('#cpValid').val('1').change();
                }
            },
            errorElement: 'label',
            errorPlacement: function errorPlacement(error, element) {
                error.attr("id", $(element).attr("name") + "_error");
                error.attr("class", "error message");

                if (element.attr('data-msg-target')) {
                    var target = element.attr('data-msg-target');
                    if (error.text() != '') {
                        error.appendTo($(target));
                    }
                    return;
                }
                error.insertAfter(element.parent().children().last());
                if (error.text() != '') {

                    ANA.ia_utag_form(fname, 'error_on_submit');
                    $('#cpValid').val('0').change();
                }
            },
            invalidHandler: function invalidHandler() {},
            submitHandler: function submitHandler(form, event) {
                event.preventDefault();
                return false;
            }
        });
    }

    function setupSliders(DEFAULT) {

        var ageSlider = {
            ticks: [DEFAULT.minAge, DEFAULT.maxAge],
            ticks_labels: [],
            min: DEFAULT.minAge,
            max: DEFAULT.maxAge,
            tooltip: 'hide'
        };
        $('#ageSlider').slider(ageSlider);

        var montantSlider = {
            ticks: [DEFAULT.minCotisation, DEFAULT.maxCotisation],
            ticks_labels: [],
            min: DEFAULT.minCotisation,
            max: DEFAULT.maxCotisation,
            tooltip: 'hide',
            step: 5
        };
        $('#montantSlider').slider(montantSlider);
    }

    function setupPie(data) {

        var propCotisations = data.totalContributions / data.epargneTotale,
            propSubventions = data.totalSubventions / data.epargneTotale,
            propInteret = data.totalRendement / data.epargneTotale,
            colors = ['#002688', '#2062d4', '#7bc2d9']; // couleurs bleu

        // si le theme est vert
        if ($('.titre-bleu h2').css('color') == "rgb(20, 144, 106)") {
            colors = ['#0d5f45', '#14906a', '#19bb89']; // couleur vert
        }

        var pieData = [{
            value: propCotisations,
            color: colors[0]
        }, {
            value: propSubventions,
            color: colors[1]
        }, {
            value: propInteret,
            color: colors[2]
        }];

        var myPie = new Chart(document.getElementById("resultatTarteReee").getContext("2d")).Doughnut(pieData, {
            animation: false,
            segmentShowStroke: false,
            percentageInnerCutout: 55
        });

        var width = 195;

        $('#resultatTarteReee').css({ width: width + 'px !important', height: width + 'px !important' });
        $('#resultatTarteReee').attr({ width: '' + width, height: '' + width });
        $('#imgResultatTarteReee').attr('src', document.getElementById("resultatTarteReee").toDataURL());
        $('#imgResultatTarteReee').hide();
        calculatorCount++;
        calculatorAnalytics(calculatorCount);
    }

    // ANALYTICS
    function calculatorAnalytics(count) {
        //ne pas faire l'analytics a chaque fois. juste au premier calcul
        if (count !== 1) {
            return;
        }
        var CP = $('#CodePostal').val().replace(' ', '').toLowerCase();
        var age = $('#Age').val();
        var contribution = $('#Montant').val();
        var AnaAge = void 0;var AnaCont = void 0;

        switch (age) {
            case '0':case '1':
                AnaAge = '0-1';
                break;
            case '2':case '3':case '4':
                AnaAge = '2-4';
                break;
            case '5':case '6':case '7':case '8':case '9':case '10':
                AnaAge = '5-10';
                break;
            case '11':case '12':case '13':case '14':case '15':
                AnaAge = '11-15';
                break;
            case '16':case '17':
                AnaAge = '15 et plus';
                break;
        }
        if (parseInt(contribution) <= 50) {
            AnaCont = '25-50';
        } else if (parseInt(contribution) < 50 && parseInt(contribution) < 101) {
            AnaCont = '51-100';
        } else {
            AnaCont = '101-500';
        }

        ANA.ia_utag_userInfo('resp_calculator', 'child_age', AnaAge); //variables (0-1; 2-4; 5-10; 10-15; 15 et plus)
        ANA.ia_utag_userInfo('resp_calculator', 'contributions_amount', AnaCont); //variables (25-50; 51-100; 101-500)
        ANA.ia_utag_userInfo('resp_calculator', 'postal_code', CP); //valeur saisie (minuscules, sans espace)

        AC.event('resp_calculate_calc_event', 'done');
        AC.contactSync( // BASE DATA
        {
            Fields: {
                'RESP_AMOUNT_CALC': contribution,
                'POSTAL_CODE': FormatCP(CP),
                'RESP_CHILD_AGE_CALC': age
            }
        });
    }

    return {
        init: init
    };
});