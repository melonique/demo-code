__cw__define(['reee-direct-cls', 'knockout', "knockout-extenders", "chart"], function (CLS, ko) {

	var _VM = function _VM(DEFAULT, callbacks) {

		self.calc = ko.observable(new CLS(DEFAULT));
		self.cpChanged = false;
		self.updateOnChange = function () {
			//si tout est
			if (self.calc().isComplete() == true) {
				calcAndUpdateGraph(calc().paramForCore(), callbacks.setupPie);
			}
		};
		self.calc().cpValid.subscribe(function (nv) {
			if (nv == 1) {
				self.updateCPinfos();
			}
		});
		self.calc().Age.subscribe(updateOnChange);
		self.calc().Montant.subscribe(updateOnChange);

		/**
   * Use the CP to get the province. That allow us to know the province policies.
   * Then, it update the ko variables and the pieChart.
   * @return {undefined}
   */
		self.updateCPinfos = function () {
			//AppelerCpInfos

			$('.loaded').css({ float: 'left', width: '100%' });

			var CP = self.calc().CodePostal();
			var retreiveEnCours = true;

			$.ajax({
				url: "/IA_API/EpargneIndividuelle/ObtenirTauxCalculateurReee",
				cache: false,
				type: "GET",
				data: {
					codePostal: CP
				},
				dataType: "json"
			}).done(function (data) {

				calc().updateTaux(data);
				retreiveEnCours = false;
				self.cpChanged = false;
				$('.loaded').css({ float: 'right', width: '0%' });

				calcAndUpdateGraph(calc().paramForCore(), callbacks.setupPie);

				setTimeout(function () {
					$('html, body').animate({
						scrollTop: $("#reee-result").offset().top - 10
					}, 1000, 'easeInOutCubic');
				}, 300);
			}).fail(function (jqXHR, textStatus) {
				console.log(jqXHR);
				alert("Une erreur est survenue lors de l'affichage de la liste des éléments : " + textStatus);
			});
		};
	};

	/**
  * calculate the Reee values using reee-core
  * @param  {object} reeeData the data returned by the "paramForCore" method from the Reee class
  * @param  {function} setupPie the callback sent to update de piechart
  * @return {undefined}
  */
	function calcAndUpdateGraph(reeeData, setupPie) {
		var request = new Entites.Public.Requete();
		request.provinceCotisant = reeeData.provinceCotisant;
		request.montantContributionMensuelle = reeeData.montantContributionMensuelle;
		request.dateDebutContribution = reeeData.dateDebutContribution;
		request.dateFinContribution = reeeData.dateFinContribution;
		request.dateNaissanceBeneficiaire = reeeData.dateNaissanceBeneficiaire;
		request.rendement = location.hash.indexOf('rendement=') > 0 ? parseInt(location.hash.split('=')[1]) : 3;

		var calculIllustrationPublic = new Affaires.Calculs.CalculIllustrationPublic();
		var response = calculIllustrationPublic.CalculerIllustration(request);

		self.calc().updateTotaux(response);
		setupPie(response);
	}

	var init = function init(vmID, DEFAULT, callbacks) {
		var VM = new _VM(DEFAULT, callbacks);
		ko.applyBindings(VM, document.getElementById(vmID));
	};

	return {
		init: init
	};
});