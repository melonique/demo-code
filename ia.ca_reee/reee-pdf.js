__cw__define(["jquery", "utils", 'analytics_ia', 'active_campaign', 'jquery.validate', 'loadingGif', "commun-calculateurs", "base64", 'jquery.fileDownload'], function ($, UTIL, a_ia, a_ac) {

	// REVIEW THIS FILE PLZ

	var pageLangue = $('html').attr('lang').substr(0, 2);

	var FormatCP = function FormatCP(value) {
		value = value.toUpperCase();
		if (value.substr(3, 1) != ' ') {
			value = value.splice(3, 0, ' ');
		}
		return value;
	};

	var msg = window.a_labels;

	var objPourPDF = function objPourPDF(param) {
		for (key in param) {
			this[key] = param[key];
		}
	};

	var objLabel = function objLabel(color, label, value) {
		this.colorId = color;
		this.label = label;
		this.value = value;
	};

	function splitBase64Url(string) {
		if (string) {
			a = string.split(',');
			return a[1];
		}
		return '';
	}

	var objField = function objField(label, value, format) {
		this.label = label;
		switch (format) {
			case 'cash':
				this.value = value.toNumber().toMoneyString();
				break;
			case 'pct':
				this.value = value + " %";
				break;
			case 'cp':
				value = value.toUpperCase();
				if (value.substr(3, 1) != ' ') {
					value = value.splice(3, 0, ' ');
				}
				this.value = value;
			case 'txt':
			default:
				this.value = value;
				break;
		}
		//  console.log(this);
	};

	var generationDuPDF = function generationDuPDF() {

		var arrFields = [],
		    arrLegend = [];

		arrFields.push(new objField($('#LabelCotisation').text(), $('#Montant').val(), 'cash'));
		arrFields.push(new objField($('#LabelAge').text(), $('#Age').val(), 'txt'));

		$('.result-legend .legend-item').each(function () {
			if ($(this).attr('style') != 'display: none;') {
				arrLegend.push(new objLabel($(this).attr('id'), $(this).find('.text').text(), $(this).find('.montant').text()));
			}
		});

		param = {
			logo: $('<a>').prop('href', $('#nav-secondaire .logo-ia img').attr('src')).prop('pathname'),
			NomFichierPdf: $('#NomFichierPdf').val(),
			titre: $('h1.hero-titre').text().trim(),
			fields: arrFields,
			result: {
				titre: $('.result-title .title').text().trim(),
				label: "",
				montant: $('.result-title .result').text().trim(),
				text: $('.result-title .text').text().trim(),
				pieChart: {
					dataURL: splitBase64Url(document.getElementById("resultatTarteReee").toDataURL()), //base64string
					legend: arrLegend,
					note: $('.result-title .btn-popover').attr('data-content') //pie chart
				} }, //result
			noteBasPage: $('#modal-hypotheses .modal-body').html()
		};
		if (param.logo.substring(0, 1) != '/') {
			param.logo = "/" + param.logo;
		}

		pdf = new objPourPDF(param);

		return pdf;
	};

	function setupPrintData() {

		var dataPDF = generationDuPDF();

		//fields
		$("#printformDataFields").val(btoa(encodeURIComponent(JSON.stringify(dataPDF.fields))));
		dataPDF.fields = null;
		//pieChart
		$("#printformPieChart1").val(encodeURIComponent(dataPDF.result.pieChart.dataURL));

		if ($("#printformPieChart1").val().length > 4000) {
			$("#printformPieChart2").val($("#printformPieChart1").val().substring(4000));
			$("#printformPieChart1").val($("#printformPieChart1").val().substring(0, 4000));
		} else {
			$("#printformPieChart2").val('');
		}

		dataPDF.result.pieChart.dataURL = null;

		//results (tout sauf les data64 des charts)
		$("#printformDataResult").val(btoa(encodeURIComponent(JSON.stringify(dataPDF.result))));
		dataPDF.result = null;
		//le reste
		$("#printformData").val(btoa(encodeURIComponent(JSON.stringify(dataPDF))));
	}

	function duplicateForTansfer() {
		setupPrintData();

		var CP = $('#CodePostal').val();
		var CPCompleted = void 0;
		if (CP.substr(3, 1) !== ' ') {
			CPCompleted = CP.substr(0, 3) + " " + CP.substr(3, 3);
		} else {
			CPCompleted = CP;
		}
		$('#Transfer_courriel').val($("#courriel").val());
		$('#Transfer_codePostal').val(CPCompleted);
		$('#Transfer_printformData').val($('#printformData').val());
		$('#Transfer_printformDataFields').val($('#printformDataFields').val());
		$('#Transfer_printformDataResult').val($('#printformDataResult').val());
		$('#Transfer_printformPieChart1').val($('#printformPieChart1').val());
		$('#Transfer_printformPieChart2').val($('#printformPieChart2').val());
		$('#Transfer_AccepteCommunication').val($('#EstAccepte').prop('checked'));
		$('#transfer_data').submit();
	};

	var loading = function loading($thing, on) {
		var loader = $thing.find('.loader-circle');
		if (on) {
			loader.css('display', 'inline-block');
		} else {
			loader.css('display', 'none');
		}
	};

	$("#telechargerPDF").on("click", function (e) {
		AC_PDFEvents();
		e.preventDefault();

		setupPrintData();
		loading($("#telechargerPDF"), true);
		$form = $('#formTelechargerPdf');
		$.fileDownload($form.prop('action'), {
			httpMethod: "POST",
			data: $form.serialize()
		}).fail(function () {
			alert('error to download');
		}).always(function () {
			loading($("#telechargerPDF"), false);
		});

		return false;
	});

	$('#transmetreResultasConseiller').on('click', function () {
		AC_PDFEvents();
		loading($("#transmetreResultasConseiller"), true);
		$('#Transfer_PageResultats').val($('#guidResults').val());
		duplicateForTansfer();
	});

	var AC_PDFEvents = function AC_PDFEvents() {
		// email, print or download result
		a_ac.event('resp_send_result_calc_event', 'done');
		a_ac.contactSync( // UPDATED DATA
		{
			Fields: {
				'RESP_AMOUNT_CALC': $('#Montant').val(),
				'POSTAL_CODE': FormatCP($('#CodePostal').val()),
				'RESP_CHILD_AGE_CALC': $('#Age').val()
			}
		});
	};
});