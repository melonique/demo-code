__cw__define(['knockout', 'knockout-extenders'], function (ko) {

		// REVIEW THIS FILE PLZ
		var Reee = function Reee(DEFAULT) {
				var self = this;

				self.CodePostal = ko.observable('');
				self.cpValid = ko.observable(0);
				self.Age = ko.observable('0').extend({ range: { min: DEFAULT.minAge, max: DEFAULT.maxAge } });
				self.Montant = ko.observable(DEFAULT.minCotisation).extend({ range: { min: DEFAULT.minCotisation, max: DEFAULT.maxCotisation } });
				self.date = new Date();
				self.Porvince = ko.observable('');
				self.TauxProvincial = ko.observable();
				self.TauxFederal = ko.observable();
				self.EstCodePostalCourt = ko.observable();

				self.totalContributions = ko.observable(0);
				self.totalSubventions = ko.observable(0);
				self.totalRendement = ko.observable(0);
				self.epargneTotale = ko.observable(0);

				/**
     * retourne si tous les champs sont complets pour être calculés
     * @return {bool}
     */
				self.isComplete = ko.computed(function () {
						return self.cpValid() == 1 && self.Porvince() !== '' && self.Age() !== '';
				}, self);

				/**
     * Update ko rate variables from reee-core
     * @param  {object} obj objet retourné par reee-core
     * @return {undefined}
     */
				self.updateTaux = function (obj) {
						self.Porvince(obj.CodeProvince);
						self.TauxProvincial(obj.TauxProvincial);
						self.TauxFederal(obj.TauxFederal);
						self.EstCodePostalCourt(obj.EstCodePostalCourt);
				};

				/**
     * update totals ko variables from reee-core
     * @param  {stirng} options.totalContributions
     * @param  {string} options.totalSubventions
     * @param  {string} options.totalRendement
     * @param  {string} options.epargneTotale
     * @return {undefined}
     */
				self.updateTotaux = function (_ref) {
						var totalContributions = _ref.totalContributions,
						    totalSubventions = _ref.totalSubventions,
						    totalRendement = _ref.totalRendement,
						    epargneTotale = _ref.epargneTotale;

						self.totalContributions(totalContributions || 0);
						self.totalSubventions(totalSubventions || 0);
						self.totalRendement(totalRendement || 0);
						self.epargneTotale(epargneTotale || 0);
				};

				/**
     * build and the object that have to be sent to reee-core
     * @return {object}
     */
				self.paramForCore = function () {
						var data = {};

						var age = self.Age();
						var dateNaissance = new Date();
						var anneeNaissance = dateNaissance.getFullYear() - age;
						dateNaissance.setFullYear(anneeNaissance);

						var dateArretCotisations = new Date(); // 31 dec qui a 17ans
						var anneeArretCotisations = anneeNaissance + 17;
						dateArretCotisations.setFullYear(anneeArretCotisations);
						dateArretCotisations.setMonth(11); //decembre
						dateArretCotisations.setDate(31); // le 31


						data.provinceCotisant = self.Porvince();
						data.montantContributionMensuelle = self.Montant();
						data.dateDebutContribution = new Date();
						data.dateNaissanceBeneficiaire = dateNaissance;
						data.dateFinContribution = dateArretCotisations;

						return data;
				};

				/**
     * Set developpeemnt variables
     */
				self.setDev = function () {
						self.CodePostal('G1k 0h1');
						self.Age('2');
						self.Montant('50');
				};
		};

		return Reee;
});